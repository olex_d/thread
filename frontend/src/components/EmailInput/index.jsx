import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Input, Button } from 'semantic-ui-react';

const EmailInput = ({ user, editEmail }) => {
  const [email, setEmail] = useState(user.email ? user.email : '');
  const [emailError, setEmailError] = useState(false);
  const [emailDisable, setEmailDisable] = useState(true);
  const emailInputRef = useRef();

  const handleEditClick = () => {
    setTimeout(() => {
      setEmailDisable(false);
      emailInputRef.current.focus();
    }, 0);
  };
  const handleSaveClick = async () => {
    const usernameErrorState = !!(await editEmail(
      user.id,
      user.email,
      email
    ));
    setEmailError(!usernameErrorState);
    setEmailDisable(usernameErrorState);
  };

  return (
    <Input
      icon="at"
      iconPosition="left"
      placeholder="Email"
      type="email"
      fluid
      disabled={emailDisable}
      value={email}
      error={emailError}
      ref={emailInputRef}
      onChange={ev => setEmail(ev.target.value)}
      action={(
        <>
          {emailDisable ? (
            <Button icon="pencil" onClick={handleEditClick} />
          ) : (
            <Button icon="check" onClick={handleSaveClick} />
          )}
        </>
      )}
    />
  );
};

EmailInput.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  editEmail: PropTypes.func.isRequired
};

export default EmailInput;
