import React, { useState, useEffect } from 'react';
import { useHistory, useRouteMatch, NavLink } from 'react-router-dom';
import Logo from 'src/components/Logo';
import { validatePasswordResetToken } from 'src/services/authService';
import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment
} from 'semantic-ui-react';

import { validatePassword } from '../../services/formValidationService';
import * as authService from '../../services/authService';

const CreateNewPassword = () => {
  const history = useHistory();
  const match = useRouteMatch('/resetPassword/:resetToken');

  const [password, setPassword] = useState('');
  const [isPasswordValid, setPasswordValid] = useState(true);
  const [isCreating, setCreating] = useState(false);

  useEffect(() => {
    const validateToken = async () => {
      const validationResult = await validatePasswordResetToken(
        match.params.resetToken
      );
      if (validationResult.isRejected) throw new Error('Invalid token');
    };
    validateToken().catch(() => {
      history.push({
        pathname: '/login',
        resetFailure: true
      });
    });
  });

  const handlePasswordChange = async () => {
    if (!validatePassword(password)) return;

    setCreating(true);
    try {
      const createResult = await authService.createPassword({
        newPassword: password,
        passwordResetToken: match.params.resetToken
      });
      if (createResult) {
        history.push({
          pathname: '/login',
          createResult
        });
      }

      throw new Error('Cannot reset password');
    } catch (e) {
      setPasswordValid(false);
    } finally {
      setCreating(false);
    }
  };

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Create new password
        </Header>
        <Form name="loginForm" size="large" onSubmit={handlePasswordChange}>
          <Segment>
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              placeholder="Password"
              type="password"
              error={!isPasswordValid}
              onChange={ev => setPassword(ev.target.value)}
              onBlur={() => setPasswordValid(validatePassword(password))}
            />
            <Button
              type="submit"
              color={isPasswordValid ? 'teal' : 'google plus'}
              fluid
              size="large"
              loading={isCreating}
              disabled={isCreating}
            >
              Create
            </Button>
          </Segment>
        </Form>
        <Message>
          Remembered password?
          {' '}
          <NavLink exact to="/login">
            Sign In
          </NavLink>
        </Message>
      </Grid.Column>
    </Grid>
  );
};

export default CreateNewPassword;
