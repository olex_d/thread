import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Label } from 'semantic-ui-react';
import buttons from '../../styles/toolbarButtons.module.scss';
import styles from './styles.module.scss';

const Dislike = ({ postId, commentId, dislikeCount, dislikeMethod }) => (
  <Label
    basic
    size="small"
    as="a"
    className={`${buttons.toolbarBtn} ${styles.dislikeButton}`}
    onClick={() => dislikeMethod(postId, commentId)}
  >
    <Icon name="thumbs down" />
    {dislikeCount}
  </Label>
);

Dislike.propTypes = {
  postId: PropTypes.string,
  commentId: PropTypes.string,
  dislikeCount: PropTypes.number.isRequired,
  dislikeMethod: PropTypes.func.isRequired
};

Dislike.defaultProps = {
  postId: null,
  commentId: null
};

export default Dislike;
