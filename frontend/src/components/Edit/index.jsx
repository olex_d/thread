import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Label } from 'semantic-ui-react';
import buttons from '../../styles/toolbarButtons.module.scss';
import styles from './styles.module.scss';

const Edit = ({ id, entityType, toggleEdit }) => (
  <Label
    basic
    size="small"
    as="a"
    className={`${buttons.toolbarBtn} ${styles.editButton}`}
    onClick={() => toggleEdit(id, entityType)}
  >
    <Icon name="pencil" />
  </Label>
);

Edit.propTypes = {
  id: PropTypes.string.isRequired,
  entityType: PropTypes.string.isRequired,
  toggleEdit: PropTypes.func.isRequired
};

export default Edit;
