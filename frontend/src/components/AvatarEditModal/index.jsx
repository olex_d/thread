import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Button, Icon, Modal } from 'semantic-ui-react';
import Cropper from 'react-easy-crop';
import Resizer from 'react-image-file-resizer';
import styles from './styles.module.scss';
import * as croppingService from '../../services/croppingService';
import * as imageService from '../../services/imageService';

const ProfileAvatarEditModal = ({
  user,
  editAvatar: avatarEdit,
  toggleAvatarEditModal: toggleAvatar
}) => {
  const DESIRED_AVATAR_SIZE = 200;

  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);
  const onCropComplete = useCallback((croppedArea, croppedPixels) => {
    setCroppedAreaPixels(croppedPixels);
  }, []);

  const [isCropping, setIsCropping] = useState(false);
  const [isUploading, setIsUploading] = useState(false);
  const [saveState, setSaveState] = useState('green');
  const setDefaultSaveState = () => {
    setSaveState('green');
  };
  const setInvalidSaveState = () => {
    setSaveState('red');
  };

  const [previewAvatar, setPreviewAvatar] = useState(null);

  const handleUploadPreview = async ({ target }) => {
    setIsUploading(true);
    Resizer.imageFileResizer(
      target.files[0],
      450,
      450,
      'JPEG',
      100,
      0,
      uri => {
        setPreviewAvatar(uri);
      },
      'base64'
    );
    setIsUploading(false);
  };

  const isSavingAvatarValid = () => {
    if (
      croppedAreaPixels.width === DESIRED_AVATAR_SIZE
      && croppedAreaPixels.height === DESIRED_AVATAR_SIZE
    ) return true;
    throw new Error('Avatar size is not 200x200');
  };
  const saveAvatar = async () => {
    try {
      if (isSavingAvatarValid()) {
        setIsCropping(true);
        const croppedAvatar = await croppingService.getCroppedImg(
          previewAvatar,
          croppedAreaPixels
        );
        const { id: imageId } = await imageService.uploadImage(croppedAvatar);
        avatarEdit(user.id, imageId);
        toggleAvatar();
      }
    } catch (e) {
      // if avatar wasn't uploaded or dimensions is different from DESIRED_AVATAR_SIZE -
      // display error state button
      setInvalidSaveState();
      setTimeout(setDefaultSaveState, 3000);
      setIsCropping(false);

      console.error(e);
    }
  };

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={() => toggleAvatar()}
    >
      <Modal.Header>
        <Icon name="image" />
        {' '}
        Update your avatar
      </Modal.Header>
      <Modal.Content>
        <Button
          fluid
          icon
          labelPosition="left"
          as="label"
          loading={isUploading}
          disabled={isUploading}
        >
          <Icon name="photo" />
          Attach avatar
          <input
            name="avatar"
            type="file"
            accept="image/*"
            onChange={handleUploadPreview}
            hidden
          />
        </Button>
        {previewAvatar && (
          <>
            <br />
            <div className={styles.cropContainer}>
              <Cropper
                image={previewAvatar}
                crop={crop}
                cropSize={{
                  width: DESIRED_AVATAR_SIZE,
                  height: DESIRED_AVATAR_SIZE
                }}
                cropShape="round"
                showGrid={false}
                onCropChange={setCrop}
                onCropComplete={onCropComplete}
              />
            </div>
            <br />
            <Button.Group fluid>
              <Button onClick={() => toggleAvatar()}>Cancel</Button>
              <Button.Or />
              <Button
                color={saveState}
                onClick={saveAvatar}
                loading={isCropping}
                disabled={isCropping}
              >
                Save
              </Button>
            </Button.Group>
          </>
        )}
      </Modal.Content>
    </Modal>
  );
};

ProfileAvatarEditModal.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  editAvatar: PropTypes.func.isRequired,
  toggleAvatarEditModal: PropTypes.func.isRequired
};

export default ProfileAvatarEditModal;
