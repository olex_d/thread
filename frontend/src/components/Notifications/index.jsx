import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Stomp } from '@stomp/stompjs';
import SockJS from 'sockjs-client';
import {
  NotificationContainer,
  NotificationManager
} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const Notifications = ({
  user,
  applyNewPost,
  applyUpdatePost,
  applyDeletePost
}) => {
  const [stompClient] = useState(Stomp.over(new SockJS('/ws')));

  useEffect(() => {
    if (!user) {
      return undefined;
    }

    stompClient.debug = () => {};
    stompClient.connect({}, () => {
      const { id } = user;

      stompClient.subscribe('/topic/new_post', message => {
        const post = JSON.parse(message.body);
        applyNewPost(post.id);
      });

      stompClient.subscribe('/topic/edit_post', message => {
        const post = JSON.parse(message.body);
        applyUpdatePost(post.id);
      });

      stompClient.subscribe('/topic/delete_post', message => {
        const postId = JSON.parse(message.body);
        applyDeletePost(postId);
      });

      stompClient.subscribe('/topic/post_reaction', message => {
        const postReaction = JSON.parse(message.body);
        if (
          postReaction.isLike
          && postReaction.postAuthorId === id
          && postReaction.userId !== id
        ) NotificationManager.info('Your post was liked!');
        applyUpdatePost(postReaction.postId);
      });

      stompClient.subscribe('/topic/new_comment', message => {
        const comment = JSON.parse(message.body);
        if (comment.postAuthorId === id && comment.user.id !== id) {
          NotificationManager.info('Your post received new comment!');
        }
        applyUpdatePost(comment.postId);
      });

      stompClient.subscribe('/topic/edit_comment', message => {
        const comment = JSON.parse(message.body);
        applyUpdatePost(comment.postId);
      });

      stompClient.subscribe('/topic/delete_comment', message => {
        const postId = JSON.parse(message.body);
        applyUpdatePost(postId);
      });

      stompClient.subscribe('/topic/comment_reaction', message => {
        const commentReaction = JSON.parse(message.body);
        if (
          commentReaction.isLike
          && commentReaction.commentAuthorId === id
          && commentReaction.userId !== id
        ) NotificationManager.info('Your comment was liked!');
        applyUpdatePost(commentReaction.commentPostId);
      });
    });

    return () => {
      stompClient.disconnect();
    };
  });

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  applyNewPost: PropTypes.func.isRequired,
  applyUpdatePost: PropTypes.func.isRequired,
  applyDeletePost: PropTypes.func.isRequired
};

export default Notifications;
