import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, Button, Icon } from 'semantic-ui-react';

const CommentInput = ({
  storeCommentMethod: storeMethod,
  postId,
  editingEntity,
  toggleEditingModal: toggleEdit
}) => {
  const [actionLabel] = useState(
    editingEntity ? 'Update' : 'Post'
  );
  const [body, setBody] = useState(
    editingEntity ? editingEntity.entity.body : ''
  );

  const handleStoreComment = async () => {
    if (!body || (editingEntity && editingEntity.entity.body === body)) return;
    try {
      await storeMethod({
        id: editingEntity?.id,
        body,
        postId: editingEntity ? editingEntity.entity.postId : postId
      });
    } catch (e) {
      return;
    }
    setBody('');

    if (editingEntity) toggleEdit(); // close editing modal window
  };

  return (
    <Form reply onSubmit={handleStoreComment}>
      <Form.TextArea
        value={body}
        placeholder="Type a comment..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type="submit" labelPosition="left" icon primary>
        <Icon name="edit" />
        {actionLabel}
      </Button>
    </Form>
  );
};

CommentInput.propTypes = {
  storeCommentMethod: PropTypes.func.isRequired,
  postId: PropTypes.string,
  editingEntity: PropTypes.objectOf(PropTypes.any),
  toggleEditingModal: PropTypes.func
};

CommentInput.defaultProps = {
  postId: null,
  editingEntity: null,
  toggleEditingModal: null
};

const mapStateToProps = rootState => ({
  editingEntity: rootState.posts.editingEntity
});

export default connect(mapStateToProps)(CommentInput);
