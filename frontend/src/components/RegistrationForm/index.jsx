import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Segment } from 'semantic-ui-react';
import {
  validatePassword,
  validateNewEmail,
  validateUsername
} from '../../services/formValidationService';

const RegistrationForm = ({ register: signOn }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [username, setUsername] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [isEmailValid, setEmailValid] = useState(true);
  const [isUsernameValid, setUsernameValid] = useState(true);
  const [isPasswordValid, setPasswordValid] = useState(true);

  const register = async () => {
    const isValid = (await validateNewEmail(email))
      && (await validateUsername(username))
      && validatePassword(password);
    if (!isValid || isLoading) {
      return;
    }
    setLoading(true);
    try {
      await signOn({ email, password, username });
    } catch {
      setLoading(false);
      setEmailValid(false);
      setUsernameValid(false);
      setPasswordValid(false);
    }
  };

  return (
    <Form name="registrationForm" size="large" onSubmit={register}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          error={!isEmailValid}
          onChange={ev => setEmail(ev.target.value)}
          onBlur={async () => setEmailValid(await validateNewEmail(email))}
        />
        <Form.Input
          fluid
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          error={!isUsernameValid}
          onChange={ev => setUsername(ev.target.value)}
          onBlur={async () => setUsernameValid(await validateUsername(username))}
        />
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          onChange={ev => setPassword(ev.target.value)}
          error={!isPasswordValid}
          onBlur={async () => setPasswordValid(validatePassword(password))}
        />
        <Button
          type="submit"
          color={
            isEmailValid && isPasswordValid && isUsernameValid
              ? 'teal'
              : 'google plus'
          }
          fluid
          size="large"
          loading={isLoading}
          disabled={isLoading}
        >
          Register
        </Button>
      </Segment>
    </Form>
  );
};

RegistrationForm.propTypes = {
  register: PropTypes.func.isRequired
};

export default RegistrationForm;
