import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Segment } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import {
  validateEmail,
  validatePassword
} from '../../services/formValidationService';

const LoginForm = ({ login }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setEmailValid] = useState(true);
  const [isPasswordValid, setPasswordValid] = useState(true);

  const handleLoginClick = async () => {
    const isValid = validateEmail(email) && validatePassword(password);
    if (!isValid || isLoading) {
      return;
    }
    setIsLoading(true);
    try {
      await login({ email, password });
    } catch {
      setIsLoading(false);
      setEmailValid(false);
      setPasswordValid(false);
    }
  };

  return (
    <Form name="loginForm" size="large" onSubmit={handleLoginClick}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Email"
          error={!isEmailValid}
          onChange={ev => setEmail(ev.target.value)}
          onBlur={() => setEmailValid(validateEmail(email))}
        />
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          error={!isPasswordValid}
          onChange={ev => setPassword(ev.target.value)}
          onBlur={() => setPasswordValid(validatePassword(password))}
        />
        <Button
          type="submit"
          color={isEmailValid && isPasswordValid ? 'teal' : 'google plus'}
          fluid
          size="large"
          loading={isLoading}
          disabled={isLoading}
        >
          Login
        </Button>
        <br />
        <NavLink exact to="/resetPassword">
          Forgot password?
        </NavLink>
      </Segment>
    </Form>
  );
};

LoginForm.propTypes = {
  login: PropTypes.func.isRequired
};

export default LoginForm;
