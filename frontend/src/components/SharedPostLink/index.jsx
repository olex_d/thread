import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Modal, Input, Icon, Segment, Button } from 'semantic-ui-react';
import * as postService from '../../services/postService';
import { validateEmail } from '../../services/formValidationService';
import styles from './styles.module.scss';

const SharedPostLink = ({ user, postId, close }) => {
  const [postLink] = useState(`${window.location.origin}/share/${postId}`);

  const [copied, setCopied] = useState(false);
  const [emailShared, setEmailShared] = useState(false);

  const [emailSharing, setEmailSharing] = useState(false);
  const [email, setEmail] = useState('');
  const [emailValid, setEmailValid] = useState(true);
  const [sharingStatus, setSharingStatus] = useState(false);

  let postLinkRef = useRef();
  const emailInputRef = useRef();

  useEffect(() => {
    if (emailSharing) {
      setEmailValid(true);
      emailInputRef.current.focus();
    }
  }, [emailSharing]);

  const copyToClipboard = e => {
    postLinkRef.select();
    document.execCommand('copy');
    e.target.focus();
    setEmailShared(false);
    setCopied(true);
  };

  const sharePostByEmail = async () => {
    if (!validateEmail(email)) return;

    setSharingStatus(true);
    try {
      await postService.sharePostByEmail({
        sender: user,
        postLink,
        recipientEmail: email
      });
      setCopied(false);
      setEmailShared(true);
    } catch (e) {
      setEmailValid(false);
    } finally {
      setSharingStatus(false);
    }
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>
          <Icon name="share alternate" />
          {' '}
          Share Post
        </span>
        {copied && (
          <span>
            <Icon color="green" name="copy" />
            Copied
          </span>
        )}
        {emailShared && (
          <span>
            <Icon color="red" name="mail" />
            Shared
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          value={postLink}
          ref={ref => {
            postLinkRef = ref;
          }}
          action={(
            <>
              <Button color="teal" icon="copy" onClick={copyToClipboard} />
              <Button
                color="google plus"
                icon="mail"
                onClick={() => setEmailSharing(!emailSharing)}
              />
            </>
          )}
        />
        {emailSharing && (
          <Segment>
            <Input
              icon="at"
              iconPosition="left"
              placeholder="Email"
              error={!emailValid}
              fluid
              onChange={ev => setEmail(ev.target.value)}
              onBlur={() => setEmailValid(validateEmail(email))}
              ref={emailInputRef}
              action={(
                <Button
                  color="google plus"
                  icon="share"
                  loading={sharingStatus}
                  disabled={sharingStatus}
                  onClick={sharePostByEmail}
                />
              )}
            />
          </Segment>
        )}
      </Modal.Content>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

SharedPostLink.defaultProps = {
  user: null
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

export default connect(mapStateToProps)(SharedPostLink);
