import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Input, Button } from 'semantic-ui-react';

const UsernameInput = ({ user, editUsername }) => {
  const [username, setUsername] = useState(user.username ? user.username : '');
  const [usernameError, setUsernameError] = useState(false);
  const [usernameDisable, setUsernameDisable] = useState(true);
  const usernameInputRef = useRef();

  const handleEditClick = () => {
    setTimeout(() => {
      setUsernameDisable(false);
      usernameInputRef.current.focus();
    }, 0);
  };
  const handleSaveClick = async () => {
    const usernameErrorState = !!(await editUsername(
      user.id,
      user.username,
      username
    ));
    setUsernameError(!usernameErrorState);
    setUsernameDisable(usernameErrorState);
  };

  return (
    <Input
      icon="user"
      iconPosition="left"
      placeholder="Username"
      type="text"
      fluid
      disabled={usernameDisable}
      value={username}
      error={usernameError}
      ref={usernameInputRef}
      onChange={ev => setUsername(ev.target.value)}
      action={(
        <>
          {usernameDisable ? (
            <Button icon="pencil" onClick={handleEditClick} />
          ) : (
            <Button icon="check" onClick={handleSaveClick} />
          )}
        </>
      )}
    />
  );
};

UsernameInput.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  editUsername: PropTypes.func.isRequired
};

export default UsernameInput;
