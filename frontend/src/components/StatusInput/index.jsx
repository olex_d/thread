import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button, Input, Label } from 'semantic-ui-react';
import styles from './styles.module.scss';

export const statusTypes = {
  header: 'header',
  profile: 'profile'
};

const StatusInput = ({ user, statusType, editStatus }) => {
  const HEADER_STATUS_LENGTH = 23;
  const [status, setStatus] = useState('');
  const [statusError, setStatusError] = useState(false);
  const [statusDisable, setStatusDisable] = useState(true);
  const statusInputRef = useRef();

  useEffect(() => {
    setStatus(user.status ? user.status : '');
  }, [user.status]);

  const handleEditClick = () => {
    setTimeout(() => {
      setStatusDisable(false);
      statusInputRef.current.focus();
    }, 0);
  };
  const handleSaveClick = async () => {
    const statusErrorState = !!(await editStatus(user.id, user.status, status));
    setStatusError(!statusErrorState);
    setStatusDisable(statusErrorState);
  };

  return (
    <>
      {statusType === statusTypes.header && (
        <Input
          placeholder="Status"
          type="text"
          disabled={statusDisable}
          value={status}
          error={statusError}
          ref={statusInputRef}
          onChange={ev => setStatus(ev.target.value)}
          transparent
          fluid={status.length >= HEADER_STATUS_LENGTH}
          action={(
            <>
              {statusDisable ? (
                <Label
                  basic
                  icon="pencil"
                  as="button"
                  onClick={handleEditClick}
                  className={styles.transparentButton}
                />
              ) : (
                <Label
                  basic
                  icon="check"
                  as="button"
                  onClick={handleSaveClick}
                  className={styles.transparentButton}
                />
              )}
            </>
          )}
        />
      )}
      {statusType === statusTypes.profile && (
        <Input
          icon="comment alternate"
          iconPosition="left"
          placeholder="Status"
          type="text"
          fluid
          disabled={statusDisable}
          value={status}
          error={statusError}
          ref={statusInputRef}
          onChange={ev => setStatus(ev.target.value)}
          action={(
            <>
              {statusDisable ? (
                <Button icon="pencil" onClick={handleEditClick} />
              ) : (
                <Button icon="check" onClick={handleSaveClick} />
              )}
            </>
          )}
        />
      )}
    </>
  );
};

StatusInput.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  editStatus: PropTypes.func.isRequired,
  statusType: PropTypes.string.isRequired
};

export default StatusInput;
