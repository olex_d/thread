import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Label } from 'semantic-ui-react';
import buttons from '../../styles/toolbarButtons.module.scss';
import styles from './styles.module.scss';

const Delete = ({ id, entityType, toggleDelete }) => (
  <Label
    basic
    size="small"
    as="a"
    className={`${buttons.toolbarBtn} ${styles.deleteButton}`}
    onClick={() => toggleDelete(id, entityType)}
  >
    <Icon name="trash" />
  </Label>
);

Delete.propTypes = {
  id: PropTypes.string.isRequired,
  entityType: PropTypes.string.isRequired,
  toggleDelete: PropTypes.func.isRequired
};

export default Delete;
