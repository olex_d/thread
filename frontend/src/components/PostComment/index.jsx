import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Label } from 'semantic-ui-react';
import buttons from '../../styles/toolbarButtons.module.scss';
import styles from './styles.module.scss';

const PostComment = ({ post, toggleExpandedPost }) => {
  const { id, commentCount } = post;
  return (
    <Label
      basic
      size="small"
      as="a"
      className={`${buttons.toolbarBtn} ${styles.commentButton}`}
      onClick={() => toggleExpandedPost(id)}
    >
      <Icon name="comment" />
      {commentCount}
    </Label>
  );
};

PostComment.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired
};

export default PostComment;

