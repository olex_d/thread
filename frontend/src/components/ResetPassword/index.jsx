import React, { useState } from 'react';
import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment
} from 'semantic-ui-react';
import { NavLink, useHistory } from 'react-router-dom';
import * as authService from 'src/services/authService';
import Logo from 'src/components/Logo';
import { validateEmail } from '../../services/formValidationService';

const ResetPassword = () => {
  const history = useHistory();

  const [email, setEmail] = useState('');
  const [isEmailValid, setEmailValid] = useState(true);
  const [isResetting, setResetting] = useState(false);

  const handleResetSubmit = async () => {
    if (!validateEmail(email)) return;

    setResetting(true);
    try {
      const currentUrl = window.location.origin;
      const resetResult = await authService.resetPassword({
        currentUrl,
        email
      });
      if (resetResult) {
        history.push({
          pathname: '/login',
          resetResult
        });
      }

      throw new Error('Cannot verify email');
    } catch (e) {
      setEmailValid(false);
    } finally {
      setResetting(false);
    }
  };

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Reset your password
        </Header>
        <Form name="resettingForm" size="large" onSubmit={handleResetSubmit}>
          <Segment>
            <Form.Input
              fluid
              icon="at"
              iconPosition="left"
              placeholder="Email"
              error={!isEmailValid}
              onChange={ev => setEmail(ev.target.value)}
              onBlur={() => setEmailValid(validateEmail(email))}
            />
            <Button
              type="submit"
              color={isEmailValid ? 'teal' : 'google plus'}
              fluid
              size="large"
              loading={isResetting}
              disabled={isResetting}
            >
              Reset
            </Button>
          </Segment>
        </Form>
        <Message>
          Remembered password?
          {' '}
          <NavLink exact to="/login">
            Sign In
          </NavLink>
        </Message>
      </Grid.Column>
    </Grid>
  );
};

export default ResetPassword;
