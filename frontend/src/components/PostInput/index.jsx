import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Form, Icon, Image, Segment } from 'semantic-ui-react';

import styles from './styles.module.scss';
import * as imageService from '../../services/imageService';

const PostInput = ({
  storePostMethod: storeMethod,
  editingEntity,
  toggleEditingModal: toggleEdit
}) => {
  const [actionLabel] = useState(editingEntity ? 'Update' : 'Post');

  const [body, setBody] = useState(
    editingEntity ? editingEntity.entity.body : ''
  );
  const [image, setImage] = useState(
    editingEntity?.entity?.image
      ? {
        imageId: editingEntity.entity.image.id,
        imageLink: editingEntity.entity.image.link
      }
      : undefined
  );
  const [isUploading, setIsUploading] = useState(false);

  const [storeButtonColor, setStoreButtonColor] = useState('blue');
  const setDefaultStoreState = () => {
    setStoreButtonColor('blue');
  };
  const setInvalidStoreState = () => {
    setStoreButtonColor('red');
  };

  const [imageUploadButtonState, setImageUploadButtonState] = useState({
    color: 'teal',
    iconName: 'image'
  });
  const setDefaultUploadState = () => {
    setImageUploadButtonState({
      color: 'teal',
      iconName: 'image'
    });
  };
  const setValidUploadState = () => {
    setImageUploadButtonState({
      color: 'green',
      iconName: 'checkmark'
    });
  };
  const setInvalidUploadState = () => {
    setImageUploadButtonState({
      color: 'red',
      iconName: 'close'
    });
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await imageService.uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
      setValidUploadState();
    } catch (e) {
      // if image wasn't uploaded - reset previous and display error state button
      setImage(undefined);
      setInvalidUploadState();
      setTimeout(setDefaultUploadState, 3000);

      console.error(e);
    } finally {
      setIsUploading(false);
    }
  };

  const postUpdated = () => {
    // if picture changed -> update
    if (
      !editingEntity?.entity?.image
      || editingEntity.entity.image.link !== image?.imageLink
    ) return true;
    if (editingEntity?.entity?.body !== body) return true; // if body changed -> update
    return false;
  };
  const handleStorePost = async () => {
    if (!body || !postUpdated()) return;
    try {
      await storeMethod({
        id: editingEntity?.id,
        imageId: image?.imageId,
        body
      });
    } catch (e) {
      setInvalidStoreState();
      setTimeout(setDefaultStoreState, 3000);
      return;
    }
    setBody('');
    setImage(undefined);
    setDefaultUploadState();

    if (editingEntity) toggleEdit(); // close editing modal window
  };

  return (
    <Segment>
      <Form onSubmit={handleStorePost}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post" />
          </div>
        )}
        <Button
          color={imageUploadButtonState.color}
          icon
          labelPosition="left"
          as="label"
          loading={isUploading}
          disabled={isUploading}
        >
          <Icon name={imageUploadButtonState.iconName} />
          Attach image
          <input
            name="image"
            type="file"
            accept="image/*"
            onChange={handleUploadFile}
            hidden
          />
        </Button>
        <Button floated="right" color={storeButtonColor} type="submit">
          {actionLabel}
        </Button>
      </Form>
    </Segment>
  );
};

PostInput.propTypes = {
  storePostMethod: PropTypes.func.isRequired,
  editingEntity: PropTypes.objectOf(PropTypes.any),
  toggleEditingModal: PropTypes.func
};

PostInput.defaultProps = {
  editingEntity: null,
  toggleEditingModal: null
};

const mapStateToProps = rootState => ({
  editingEntity: rootState.posts.editingEntity
});

export default connect(mapStateToProps)(PostInput);
