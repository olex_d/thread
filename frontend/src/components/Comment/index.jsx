import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reactiveEntityType } from '../../services/reactionsDictionary';
import { likeComment, dislikeComment } from './actions';
import Like from '../Like';
import Dislike from '../Dislike';
import Edit from '../Edit';
import Delete from '../Delete';

import buttons from '../../styles/toolbarButtons.module.scss';
import styles from './styles.module.scss';

const Comment = ({
  likeComment: like,
  dislikeComment: dislike,
  comment: { id, postId, body, likeCount, dislikeCount, createdAt, user },
  toggleEditingModal: toggleEdit,
  toggleDeletingModal: toggleDelete
}) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={getUserImgLink(user.image)} />
    <CommentUI.Content>
      <CommentUI.Author as="a" content={user.username} />
      <CommentUI.Metadata content={moment(createdAt).fromNow()} />
      <CommentUI.Metadata content={user.status} style={{ display: 'block' }} />
      <CommentUI.Text content={body} />
      <CommentUI.Actions>
        <div className={buttons.contentActions}>
          <div>
            <CommentUI.Action as="span">
              <Like
                postId={postId}
                commentId={id}
                likeCount={likeCount}
                entityType={reactiveEntityType.COMMENT}
                likeMethod={like}
              />
            </CommentUI.Action>
            <CommentUI.Action as="span">
              <Dislike
                postId={postId}
                commentId={id}
                dislikeCount={dislikeCount}
                dislikeMethod={dislike}
              />
            </CommentUI.Action>
          </div>
          <div>
            <CommentUI.Action as="span">
              {toggleEdit && (
                <Edit
                  id={id}
                  entityType={reactiveEntityType.COMMENT}
                  toggleEdit={toggleEdit}
                />
              )}
            </CommentUI.Action>
            <CommentUI.Action as="span">
              {toggleDelete && (
                <Delete
                  id={id}
                  entityType={reactiveEntityType.COMMENT}
                  toggleDelete={toggleDelete}
                />
              )}
            </CommentUI.Action>
          </div>
        </div>
      </CommentUI.Actions>
    </CommentUI.Content>
  </CommentUI>
);

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  toggleEditingModal: PropTypes.func,
  toggleDeletingModal: PropTypes.func
};

Comment.defaultProps = {
  toggleEditingModal: null,
  toggleDeletingModal: null
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts
});

const actions = {
  likeComment,
  dislikeComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Comment);
