import * as commentService from 'src/services/commentService';
import { reaction } from '../../services/reactionsDictionary';

// Comments interactions

export const addComment = request => async () => {
  await commentService.addComment(request);
};

export const editComment = request => async () => {
  await commentService.editComment(request);
};

export const deleteComment = commentId => async () => {
  await commentService.deleteComment({ id: commentId });
};

// Comments reactions

export const likeComment = (postId, commentId) => async () => {
  await commentService.reactToComment(commentId, reaction.LIKE);
};

export const dislikeComment = (postId, commentId) => async () => {
  await commentService.reactToComment(commentId, reaction.DISLIKE);
};
