import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Label } from 'semantic-ui-react';
import buttons from '../../styles/toolbarButtons.module.scss';
import styles from './styles.module.scss';

const PostShare = ({ postId, sharePost }) => (
  <Label
    basic
    size="small"
    as="a"
    className={`${buttons.toolbarBtn} ${styles.shareButton}`}
    onClick={() => sharePost(postId)}
  >
    <Icon name="share alternate" />
  </Label>
);

PostShare.propTypes = {
  postId: PropTypes.string.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default PostShare;
