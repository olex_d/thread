import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Icon, Modal, Button } from 'semantic-ui-react';
import {
  deletePost
} from 'src/containers/Thread/actions';
import { deleteComment } from 'src/components/Comment/actions';

import { bindActionCreators } from 'redux';
import { reactiveEntityType } from '../../services/reactionsDictionary';
import Spinner from '../Spinner';

const DeleteModal = ({
  deleteEntity,
  deletePost: delPost,
  deleteComment: delComment,
  toggleDeletingModal: toggleDelete
}) => {
  const handleDeleting = async () => {
    if (deleteEntity.type === reactiveEntityType.POST) await delPost(deleteEntity.id);
    else await delComment(deleteEntity.id);

    toggleDelete(deleteEntity.id, deleteEntity.type);
  };

  return (
    <Modal dimmer="blurring" open size="tiny" onClose={() => toggleDelete()}>
      {deleteEntity.id ? (
        <>
          <Modal.Header>
            <Icon name="trash" />
            {' '}
            Do you really want to delete this
            {' '}
            <span>
              {deleteEntity.type === reactiveEntityType.POST ? 'post' : 'comment'}
            </span>
            ?
          </Modal.Header>
          <Modal.Actions>
            <Button color="blue" onClick={() => toggleDelete(deleteEntity.id, deleteEntity.type)} inverted>
              <Icon name="close" />
              {' '}
              Cancel
            </Button>
            <Button color="red" onClick={handleDeleting} inverted>
              <Icon name="check" />
              {' '}
              Delete
            </Button>
          </Modal.Actions>
        </>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

DeleteModal.propTypes = {
  deleteEntity: PropTypes.objectOf(PropTypes.any).isRequired,
  deletePost: PropTypes.func,
  deleteComment: PropTypes.func,
  toggleDeletingModal: PropTypes.func.isRequired
};

DeleteModal.defaultProps = {
  deletePost: null,
  deleteComment: null
};

const mapStateToProps = rootState => ({
  deleteEntity: rootState.posts.deleteEntity
});

const actions = {
  deletePost,
  deleteComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DeleteModal);
