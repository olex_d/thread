import { UNSET_ALL_LIKERS, LOAD_MORE_LIKERS } from './actionTypes';
import * as postService from '../../services/postService';
import * as commentService from '../../services/commentService';
import { reactiveEntityType } from '../../services/reactionsDictionary';

const addMoreLikersAction = likers => ({
  type: LOAD_MORE_LIKERS,
  likers
});

const removeLikersAction = () => ({
  type: UNSET_ALL_LIKERS
});

export const loadMoreLikers = (id, entityType, from, count) => async (
  dispatch,
  getRootState
) => {
  const {
    likers: { likers }
  } = getRootState();
  let loadedLikers;
  if (entityType === reactiveEntityType.POST) {
    loadedLikers = await postService.getAllPostLikers({ id, from, count });
  } else {
    loadedLikers = await commentService.getAllCommentLikers({ id, from, count });
  }
  const filteredLikers = loadedLikers.filter(
    liker => !(likers && likers.some(loadedLiker => liker.id === loadedLiker.id))
  );
  dispatch(addMoreLikersAction(filteredLikers));
};

export const unsetLikers = () => async dispatch => {
  dispatch(removeLikersAction());
};
