import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Icon, Label, Popup, Segment, Image, Loader } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import InfiniteScroll from 'react-infinite-scroller';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { loadMoreLikers, unsetLikers } from 'src/components/Like/actions';
import { reactiveEntityType } from '../../services/reactionsDictionary';
import buttons from '../../styles/toolbarButtons.module.scss';
import styles from './styles.module.scss';

const likersPaging = {
  from: 0,
  count: 10
};

const Like = ({
  postId,
  commentId,
  likeCount,
  entityType,
  likeMethod,
  likers = [],
  loadMoreLikers: loadMore,
  unsetLikers: unset,
  hasMoreLikers
}) => {
  const [scrollParentRef, setScrollParentRef] = useState(null);

  const getMoreLikers = () => {
    const { from, count } = likersPaging;
    if (entityType === reactiveEntityType.POST) loadMore(postId, entityType, from, count);
    else loadMore(commentId, entityType, from, count);
  };
  const distinctLikers = [
    ...new Map(likers.map(liker => [liker.id, liker])).values()
  ];

  return (
    <Popup
      basic
      inverted
      hideOnScroll
      size="tiny"
      hoverable
      position="right center"
      onMount={() => {
        unset();
      }}
      trigger={(
        <Label
          basic
          size="small"
          as="a"
          className={`${buttons.toolbarBtn} ${styles.likeButton}`}
          onClick={() => likeMethod(postId, commentId)}
        >
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
      )}
    >
      <>
        <Popup.Header content="Liked by:" />
        <Popup.Content>
          <div ref={ref => setScrollParentRef(ref)}>
            <InfiniteScroll
              pageStart={0}
              loadMore={getMoreLikers}
              hasMore={hasMoreLikers}
              useWindow={false}
              getScrollParent={() => scrollParentRef}
              loader={(
                <Loader
                  active
                  inline="centered"
                  size="tiny"
                  className={styles.likersLoader}
                  key={0}
                />
              )}
            >
              <Segment.Group>
                {distinctLikers.map(liker => (
                  <Segment inverted textAlign="left" key={liker.id}>
                    <Image
                      avatar
                      spaced="right"
                      src={getUserImgLink(liker?.image)}
                    />
                    {liker.username}
                  </Segment>
                ))}
              </Segment.Group>
            </InfiniteScroll>
          </div>
        </Popup.Content>
      </>
    </Popup>
  );
};

Like.propTypes = {
  postId: PropTypes.string,
  commentId: PropTypes.string,
  likeCount: PropTypes.number.isRequired,
  entityType: PropTypes.string.isRequired,
  likeMethod: PropTypes.func.isRequired,
  likers: PropTypes.arrayOf(PropTypes.object),
  loadMoreLikers: PropTypes.func.isRequired,
  hasMoreLikers: PropTypes.bool,
  unsetLikers: PropTypes.func.isRequired
};

Like.defaultProps = {
  postId: null,
  commentId: null,
  likers: [],
  hasMoreLikers: true
};

const mapStateToProps = rootState => ({
  likers: rootState.likers.likers,
  hasMoreLikers: rootState.likers.hasMoreLikers
});

const actions = {
  loadMoreLikers,
  unsetLikers
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Like);
