import { LOAD_MORE_LIKERS, UNSET_ALL_LIKERS } from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case LOAD_MORE_LIKERS:
      return {
        ...state,
        likers: [...(state.likers || []), ...action.likers],
        hasMoreLikers: Boolean(action.likers.length)
      };
    case UNSET_ALL_LIKERS:
      return {
        ...state,
        likers: [],
        hasMoreLikers: true
      };
    default:
      return state;
  }
};
