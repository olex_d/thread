import React from 'react';
import PropTypes from 'prop-types';
import { Checkbox, Grid } from 'semantic-ui-react';

const PostsFilter = ({ showOwnPosts, hideOwnPosts, showLiked }) => (
  <Grid columns="equal">
    <Grid.Row>
      <Grid.Column>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts.state}
          onChange={showOwnPosts.toggle}
        />
      </Grid.Column>
      <Grid.Column>
        <Checkbox
          toggle
          label="Hide my posts"
          checked={hideOwnPosts.state}
          onChange={hideOwnPosts.toggle}
        />
      </Grid.Column>
      <Grid.Column>
        <Checkbox
          toggle
          label="Liked by me"
          checked={showLiked.state}
          onChange={showLiked.toggle}
        />
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

PostsFilter.propTypes = {
  showOwnPosts: PropTypes.objectOf(PropTypes.any).isRequired,
  hideOwnPosts: PropTypes.objectOf(PropTypes.any).isRequired,
  showLiked: PropTypes.objectOf(PropTypes.any).isRequired
};

export default PostsFilter;
