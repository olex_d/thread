import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Dimmer, Icon, Image } from 'semantic-ui-react';
import { getUserImgLink } from '../../helpers/imageHelper';

const ProfileAvatarInput = ({ user, toggleAvatarEditModal: toggleAvatar }) => {
  const [imageHovered, setImageHovered] = useState(false);

  return (
    <Dimmer.Dimmable
      as={Image}
      onMouseEnter={() => setImageHovered(true)}
      onMouseLeave={() => setImageHovered(false)}
      blurring
      circular
      dimmed={imageHovered}
    >
      <Image centered src={getUserImgLink(user.image)} size="medium" circular />
      <Icon name={imageHovered ? 'photo' : 'pencil'} color="grey" />
      <Dimmer
        icon="photo"
        active={imageHovered}
        verticalAlign="bottom"
        onClickOutside={() => toggleAvatar(user.image)}
      />
    </Dimmer.Dimmable>
  );
};

ProfileAvatarInput.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleAvatarEditModal: PropTypes.func.isRequired
};

export default ProfileAvatarInput;
