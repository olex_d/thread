import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
  Header as HeaderUI,
  Image,
  Grid,
  Icon,
  Button,
  Responsive
} from 'semantic-ui-react';
import StatusInput, { statusTypes } from '../../components/StatusInput';

import styles from './styles.module.scss';

const Header = ({ user, logout, editStatus: statusEdit }) => (
  <div className={styles.headerWrp}>
    <Grid centered verticalAlign="middle" container columns={2}>
      <Grid.Column>
        {user && (
          <NavLink exact to="/">
            <HeaderUI as="h3">
              <Responsive as={React.Fragment} minWidth={703}>
                <Image circular src={getUserImgLink(user.image)} />
              </Responsive>
              <HeaderUI.Content as="div" style={{ width: '65%' }}>
                {user.username}
                <HeaderUI.Subheader>
                  <div onClick={event => event.preventDefault()}>
                    <StatusInput
                      user={user}
                      statusType={statusTypes.header}
                      editStatus={statusEdit}
                    />
                  </div>
                </HeaderUI.Subheader>
              </HeaderUI.Content>
            </HeaderUI>
          </NavLink>
        )}
      </Grid.Column>
      <Grid.Column textAlign="right">
        <NavLink
          exact
          activeClassName="active"
          to="/profile"
          className={styles.menuBtn}
        >
          <Icon name="user circle" size="large" />
        </NavLink>
        <Button
          basic
          icon
          type="button"
          className={`${styles.menuBtn} ${styles.logoutBtn}`}
          onClick={logout}
        >
          <Icon name="log out" size="large" />
        </Button>
      </Grid.Column>
    </Grid>
  </div>
);

Header.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  logout: PropTypes.func.isRequired,
  editStatus: PropTypes.func.isRequired
};

export default Header;
