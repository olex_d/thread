import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  likePost,
  dislikePost,
  toggleEditingModal,
  toggleDeletingModal,
  toggleExpandedPost
} from 'src/containers/Thread/actions';
import { addComment } from 'src/components/Comment/actions';
import Post from 'src/containers/Post';
import Comment from 'src/components/Comment';
import CommentInput from 'src/components/CommentInput';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  userId,
  likePost: like,
  dislikePost: dislike,
  toggleEditingModal: toggleEdit,
  toggleDeletingModal: toggleDelete,
  toggleExpandedPost: toggleExpand,
  sharePost,
  addComment: add
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggleExpand()}>
    {post ? (
      <Modal.Content>
        <Post
          post={post}
          likePost={like}
          dislikePost={dislike}
          toggleEditingModal={post.user.id === userId ? toggleEdit : null} // allow to edit only own posts
          toggleDeletingModal={post.user.id === userId ? toggleDelete : null} // allow to delete only own posts
          toggleExpandedPost={toggleExpand}
          sharePost={sharePost}
        />
        <CommentUI.Group style={{ maxWidth: '100%' }}>
          <Header as="h3" dividing>
            Comments
          </Header>
          {post.comments
            && post.comments
              .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
              .map(comment => (
                <Comment
                  key={comment.id}
                  comment={comment}
                  toggleEditingModal={
                    comment.user.id === userId ? toggleEdit : null // allow to edit only own comments
                  }
                  toggleDeletingModal={
                    comment.user.id === userId ? toggleDelete : null // allow to delete only own comments
                  }
                />
              ))}
          <CommentInput postId={post.id} storeCommentMethod={add} />
        </CommentUI.Group>
      </Modal.Content>
    ) : (
      <Spinner />
    )}
  </Modal>
);

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleEditingModal: PropTypes.func.isRequired,
  toggleDeletingModal: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired
};

ExpandedPost.defaultProps = {
  userId: undefined
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  likePost,
  dislikePost,
  toggleEditingModal,
  toggleDeletingModal,
  toggleExpandedPost,
  addComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ExpandedPost);
