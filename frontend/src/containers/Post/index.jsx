import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Card, Image } from 'semantic-ui-react';
import moment from 'moment';
import { reactiveEntityType } from '../../services/reactionsDictionary';
import buttons from '../../styles/toolbarButtons.module.scss';
import {
  likePost,
  dislikePost,
  toggleExpandedPost
} from '../Thread/actions';
import Like from '../../components/Like';
import PostComment from '../../components/PostComment';
import PostShare from '../../components/PostShare';
import Edit from '../../components/Edit';
import Delete from '../../components/Delete';
import Dislike from '../../components/Dislike';

const Post = ({
  post,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggleExpand,
  toggleEditingModal: toggleEdit,
  toggleDeletingModal: toggleDelete,
  sharePost: share
}) => {
  const { id, image, body, likeCount, dislikeCount, user, createdAt } = post;
  const date = moment(createdAt).fromNow();
  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>{body}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <div className={buttons.contentActions}>
          <div>
            <Like
              postId={id}
              likeCount={likeCount}
              entityType={reactiveEntityType.POST}
              likeMethod={like}
            />
            <Dislike
              postId={id}
              dislikeCount={dislikeCount}
              dislikeMethod={dislike}
            />
            <PostComment post={post} toggleExpandedPost={toggleExpand} />
            <PostShare postId={id} sharePost={share} />
          </div>
          <div>
            {toggleEdit && (
              <Edit
                id={id}
                entityType={reactiveEntityType.POST}
                toggleEdit={toggleEdit}
              />
            )}
            {toggleDelete && (
              <Delete
                id={id}
                entityType={reactiveEntityType.POST}
                toggleDelete={toggleDelete}
              />
            )}
          </div>
        </div>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditingModal: PropTypes.func,
  toggleDeletingModal: PropTypes.func,
  sharePost: PropTypes.func.isRequired
};

Post.defaultProps = {
  toggleEditingModal: null,
  toggleDeletingModal: null
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts
});

const actions = {
  likePost,
  dislikePost,
  toggleExpandedPost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Post);
