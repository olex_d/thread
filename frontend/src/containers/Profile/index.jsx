import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Grid } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import {
  editUsername,
  editEmail,
  editAvatar,
  editStatus,
  toggleAvatarEditModal
} from './actions';
import Avatar from '../../components/ProfileAvatar';
import UsernameInput from '../../components/UsernameInput';
import AvatarEditModal from '../../components/AvatarEditModal';
import StatusInput, { statusTypes } from '../../components/StatusInput';
import EmailInput from '../../components/EmailInput';

const Profile = ({
  user,
  editingAvatar,
  editAvatar: avatarEdit,
  editUsername: nameEdit,
  editEmail: emailEdit,
  editStatus: statusEdit,
  toggleAvatarEditModal: toggleAvatar
}) => (
  <Grid container textAlign="center" style={{ paddingTop: 30 }}>
    <Grid.Column mobile={11} tablet={7} computer={6}>
      <Avatar user={user} toggleAvatarEditModal={toggleAvatar} />
      <br />
      <br />
      <UsernameInput user={user} editUsername={nameEdit} />
      <br />
      <EmailInput user={user} editEmail={emailEdit} />
      <br />
      <StatusInput
        user={user}
        statusType={statusTypes.profile}
        editStatus={statusEdit}
      />
    </Grid.Column>
    {editingAvatar !== undefined && (
      <AvatarEditModal
        user={user}
        editAvatar={avatarEdit}
        toggleAvatarEditModal={toggleAvatar}
      />
    )}
  </Grid>
);

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  editingAvatar: PropTypes.objectOf(PropTypes.any),
  editUsername: PropTypes.func.isRequired,
  editAvatar: PropTypes.func.isRequired,
  editEmail: PropTypes.func.isRequired,
  editStatus: PropTypes.func.isRequired,
  toggleAvatarEditModal: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {},
  editingAvatar: undefined
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user,
  editingAvatar: rootState.profile.editingAvatar
});

const actions = {
  editAvatar,
  editUsername,
  editEmail,
  editStatus,
  toggleAvatarEditModal
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
