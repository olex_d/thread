import * as authService from 'src/services/authService';
import * as profileService from '../../services/profileService';
import { SET_EDIT_AVATAR, SET_USER } from './actionTypes';
import {
  validateNewEmail,
  validateUsername
} from '../../services/formValidationService';

// Redux actions

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const setEditAvatarAction = avatar => async dispatch => dispatch({
  type: SET_EDIT_AVATAR,
  avatar
});

// Auth service methods

const handleAuthResponse = authResponsePromise => async (
  dispatch,
  getRootState
) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  await setUser(user)(dispatch, getRootState);
};

// Users updating methods

export const editAvatar = (id, imageId) => async (dispatch, getRootState) => {
  await profileService.editUser({
    id,
    imageId
  });
  await loadCurrentUser()(dispatch, getRootState);
  return true;
};

// new username invalid if it is empty or already exist
export const editUsername = (
  id,
  oldUsername,
  newUsername
) => async (dispatch, getRootState) => {
  if (oldUsername.toUpperCase() === newUsername.toUpperCase()) return true;
  if (!await validateUsername(newUsername)) return false;

  await profileService.editUser({
    id,
    username: newUsername
  });
  await loadCurrentUser()(dispatch, getRootState);
  return true;
};

// new email invalid if it is empty, already exist or not in email format
export const editEmail = (
  id,
  oldEmail,
  newEmail
) => async (dispatch, getRootState) => {
  if (oldEmail.toUpperCase() === newEmail.toUpperCase()) return true;
  if (!await validateNewEmail(newEmail)) return false;
  await profileService.editUser({
    id,
    email: newEmail
  });
  await loadCurrentUser()(dispatch, getRootState);
  return true;
};

export const editStatus = (
  id,
  oldStatus,
  newStatus
) => async (dispatch, getRootState) => {
  if (oldStatus.toUpperCase() === newStatus.toUpperCase()) return true;
  await profileService.editUser({
    id,
    status: newStatus
  });
  await loadCurrentUser()(dispatch, getRootState);
  return true;
};

// Modals toggling methods

export const toggleAvatarEditModal = avatar => async dispatch => {
  dispatch(setEditAvatarAction(avatar));
};
