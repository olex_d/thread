import { SET_EDIT_AVATAR, SET_USER } from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthorized: Boolean(action.user?.id),
        isLoading: false
      };
    case SET_EDIT_AVATAR:
      return {
        ...state,
        editingAvatar: action.avatar
      };
    default:
      return state;
  }
};
