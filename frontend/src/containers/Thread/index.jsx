import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ExpandedPost from 'src/containers/ExpandedPost';
import ThreadEditModal from 'src/containers/ThreadContentEditModal';
import Post from 'src/containers/Post';
import PostInput from 'src/components/PostInput';
import PostsFilters from 'src/components/PostsFilters';
import SharedPostLink from 'src/components/SharedPostLink';
import { Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts,
  loadMorePosts,
  addPost,
  toggleEditingModal,
  toggleDeletingModal
} from './actions';

import styles from './styles.module.scss';
import DeleteModal from '../../components/DeleteModal';

export const criteria = {
  UserPosts: 'UserPosts',
  NotUserPosts: 'NotUserPosts',
  UserLikedPosts: 'UserLikedPosts'
};

const PAGINATION_STEP = 10;

const postsFilter = {
  userId: undefined,
  from: 0,
  count: PAGINATION_STEP,
  criterion: []
};

const Thread = ({
  userId,
  posts = [],
  loadPosts: load,
  loadMorePosts: loadMore,
  hasMorePosts,
  expandedPost,
  editingEntity,
  deleteEntity,
  addPost: createPost,
  toggleEditingModal: toggleEdit,
  toggleDeletingModal: toggleDelete
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [hideOwnPosts, setHideOwnPosts] = useState(false);
  const [showLiked, setShowLiked] = useState(false);

  const distinctPosts = [
    ...new Map(posts.map(post => [post.id, post])).values()
  ];

  useEffect(() => {
    const updateFilteredPosts = async () => {
      postsFilter.criterion = [];
      if (showOwnPosts) postsFilter.criterion.push(criteria.UserPosts);
      if (hideOwnPosts) postsFilter.criterion.push(criteria.NotUserPosts);
      if (showLiked) postsFilter.criterion.push(criteria.UserLikedPosts);

      postsFilter.userId = userId;
      postsFilter.from = 0;
      postsFilter.count = PAGINATION_STEP;

      await load(postsFilter);

      postsFilter.from += PAGINATION_STEP; // for the next scroll
      postsFilter.count += PAGINATION_STEP;
    };
    updateFilteredPosts().catch(error => console.error(error));
  }, [showOwnPosts, hideOwnPosts, showLiked, userId, load]);

  const toggleShowOwnPosts = () => {
    setHideOwnPosts(false);
    setShowOwnPosts(!showOwnPosts);
  };

  const toggleHideOwnPosts = () => {
    setShowOwnPosts(false);
    setHideOwnPosts(!hideOwnPosts);
  };

  const toggleLikedPosts = () => {
    setShowLiked(!showLiked);
  };

  const getMorePosts = () => {
    loadMore(postsFilter);

    postsFilter.from += PAGINATION_STEP; // for the next scroll
    postsFilter.count += PAGINATION_STEP;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <PostInput storePostMethod={createPost} />
      </div>
      <div className={styles.toolbar}>
        <PostsFilters
          showOwnPosts={{ state: showOwnPosts, toggle: toggleShowOwnPosts }}
          hideOwnPosts={{ state: hideOwnPosts, toggle: toggleHideOwnPosts }}
          showLiked={{ state: showLiked, toggle: toggleLikedPosts }}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {distinctPosts.map(post => (
          <Post
            post={post}
            toggleEditingModal={post.user.id === userId ? toggleEdit : null} // allow to edit only own posts
            toggleDeletingModal={post.user.id === userId ? toggleDelete : null} // allow to delete only own posts
            sharePost={sharePost}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {editingEntity && <ThreadEditModal toggleEditingModal={toggleEdit} />}
      {deleteEntity && <DeleteModal toggleDeletingModal={toggleDelete} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  editingEntity: PropTypes.objectOf(PropTypes.any),
  deleteEntity: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  toggleEditingModal: PropTypes.func.isRequired,
  toggleDeletingModal: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  editingEntity: undefined,
  deleteEntity: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  editingEntity: rootState.posts.editingEntity,
  deleteEntity: rootState.posts.deleteEntity,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  addPost,
  toggleEditingModal,
  toggleDeletingModal
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Thread);
