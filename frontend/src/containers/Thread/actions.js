import * as postService from 'src/services/postService';
import * as commentService from '../../services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  TOGGLE_EDITING_ENTITY,
  TOGGLE_DELETE_ENTITY
} from './actionTypes';
import {
  reactiveEntityType,
  reaction
} from '../../services/reactionsDictionary';

// Redux actions

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setEditingPostAction = (id, editingEntity, entityType) => ({
  type: TOGGLE_EDITING_ENTITY,
  editingEntity: { id, entity: editingEntity, type: entityType }
});

const toggleDeleteEntityAction = (id, entityType) => ({
  type: TOGGLE_DELETE_ENTITY,
  deleteEntity: { id, type: entityType }
});

// Posts updating methods

export const updateListPost = (postId, mapListPost) => async (
  dispatch,
  getRootState
) => {
  const {
    posts: { posts }
  } = getRootState();
  const updatedPosts = posts.map(post => (post.id !== postId ? post : mapListPost(post)));
  dispatch(setPostsAction(updatedPosts));
};

export const updateExpandedPost = (postId, mapExpandedPost) => async (
  dispatch,
  getRootState
) => {
  const {
    posts: { expandedPost }
  } = getRootState();
  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapExpandedPost(expandedPost)));
  }
};

export const fullPostUpdate = (postId, mapPost) => async (
  dispatch,
  getRootState
) => {
  await updateListPost(postId, mapPost)(dispatch, getRootState);
  await updateExpandedPost(postId, mapPost)(dispatch, getRootState);
};

// WebSocket synchronization

export const applyNewPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const applyUpdatePost = postId => async (dispatch, getRootState) => {
  const updatedPost = await postService.getPost(postId);
  const mapPostUpdate = () => ({
    ...updatedPost
  });
  await fullPostUpdate(postId, mapPostUpdate)(dispatch, getRootState);
};

export const applyDeletePost = postId => async (dispatch, getRootState) => {
  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const deletingPosts = [...posts];
  const deletingIndex = deletingPosts.map(post => post.id).indexOf(postId); // remove post from general list
  deletingPosts.splice(deletingIndex, 1);
  dispatch(setPostsAction(deletingPosts));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(undefined));
  }
};

// Uploading from server

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePostsAction(filteredPosts));
};

// Posts interactions

export const addPost = post => async () => {
  await postService.addPost(post);
};

export const editPost = post => async () => {
  await postService.editPost(post);
};

export const deletePost = postId => async () => {
  await postService.deletePost({ id: postId });
};

// Posts reactions

export const likePost = postId => async () => {
  await postService.reactToPost(postId, reaction.LIKE);
};

export const dislikePost = postId => async () => {
  await postService.reactToPost(postId, reaction.DISLIKE);
};

// Modals toggling methods

export const toggleExpandedPost = postId => async dispatch => {
  const expandedPost = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(expandedPost));
};

export const toggleEditingModal = (id, entityType) => async dispatch => {
  let editingEntity;
  if (entityType === reactiveEntityType.POST) editingEntity = id ? await postService.getPost(id) : undefined;
  else editingEntity = id ? await commentService.getComment(id) : undefined;
  dispatch(setEditingPostAction(id, editingEntity, entityType));
};

export const toggleDeletingModal = (id, entityType) => async dispatch => {
  dispatch(toggleDeleteEntityAction(id, entityType));
};
