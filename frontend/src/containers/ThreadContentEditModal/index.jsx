import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Icon, Modal } from 'semantic-ui-react';
import Spinner from 'src/components/Spinner';
import PostInput from 'src/components/PostInput';
import { bindActionCreators } from 'redux';
import { editPost } from '../Thread/actions';
import { editComment } from '../../components/Comment/actions';
import { reactiveEntityType } from '../../services/reactionsDictionary';
import CommentInput from '../../components/CommentInput';

const ThreadEditModal = ({
  editingEntity,
  editPost: postEdit,
  editComment: commentEdit,
  toggleEditingModal: toggleEdit
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggleEdit()}>
    {editingEntity ? (
      <>
        <Modal.Header>
          <Icon name="pencil" />
          {' '}
          Update your
          {' '}
          <span>
            {editingEntity.type === reactiveEntityType.POST
              ? 'post'
              : 'comment'}
          </span>
        </Modal.Header>
        <Modal.Content>
          {editingEntity.type === reactiveEntityType.POST
            ? <PostInput storePostMethod={postEdit} toggleEditingModal={toggleEdit} />
            : <CommentInput storeCommentMethod={commentEdit} toggleEditingModal={toggleEdit} />}
        </Modal.Content>
      </>
    ) : (
      <Spinner />
    )}
  </Modal>
);

ThreadEditModal.propTypes = {
  editingEntity: PropTypes.objectOf(PropTypes.any).isRequired,
  editPost: PropTypes.func.isRequired,
  editComment: PropTypes.func.isRequired,
  toggleEditingModal: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  editingEntity: rootState.posts.editingEntity
});

const actions = {
  editPost,
  editComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ThreadEditModal);
