import callWebApi from 'src/helpers/webApiHelper';
import { reaction } from './reactionsDictionary';

export const getAllPosts = async filter => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'GET',
    query: filter
  });
  return response.json();
};

export const getPost = async id => {
  const response = await callWebApi({
    endpoint: `/api/posts/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const addPost = async request => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'POST',
    request
  });
  return response.json();
};

export const editPost = async request => {
  const response = await callWebApi({
    endpoint: `/api/posts/${request.id}`,
    type: 'PUT',
    request
  });
  return response.json();
};

export const deletePost = async request => {
  await callWebApi({
    endpoint: `/api/posts/${request.id}`,
    type: 'DELETE'
  });
};

export const getAllPostLikers = async request => {
  const response = await callWebApi({
    endpoint: `/api/posts/${request.id}/${request.from}/${request.count}`,
    type: 'GET'
  });
  return response.json();
};

export const reactToPost = async (postId, postReaction) => {
  const response = await callWebApi({
    endpoint: '/api/postreaction',
    type: 'PUT',
    request: {
      postId,
      isLike: postReaction === reaction.LIKE
    }
  });
  return response.json();
};

export const sharePostByEmail = async request => {
  await callWebApi({
    endpoint: '/api/posts/shareByEmail',
    type: 'POST',
    request: {
      sender: request.sender,
      postLink: request.postLink,
      recipientEmail: request.recipientEmail
    }
  });
};
