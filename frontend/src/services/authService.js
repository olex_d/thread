import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const resetPassword = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/resetPassword',
    type: 'POST',
    request
  });
  return response.json();
};

export const validatePasswordResetToken = async token => {
  const response = await callWebApi({
    endpoint: `/api/passwordToken?token=${token}`,
    type: 'GET'
  });
  return response.json();
};

export const createPassword = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/createPassword',
    type: 'POST',
    request
  });
  return response.json();
};
