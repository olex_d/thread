import callWebApi from '../helpers/webApiHelper';

export const getUser = async id => {
  const response = await callWebApi({
    endpoint: `/api/user/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const getUsersShortData = async () => {
  const response = await callWebApi({
    endpoint: '/api/user/shortdata',
    type: 'GET'
  });
  return response.json();
};

export const editUser = async request => {
  const response = await callWebApi({
    endpoint: `/api/user/${request.id}`,
    type: 'PUT',
    request
  });
  return response.json();
};
