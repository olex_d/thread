import validator from 'validator';
// eslint-disable-next-line camelcase
import { sha3_256 } from 'js-sha3';
import { getUsersShortData } from './profileService';

const MAX_USERNAME_LENGTH = 26;
const MAX_PASSWORD_LENGTH = 40;

let SHORT_USERS_DATA;

const updateUsersData = async () => {
  SHORT_USERS_DATA = await getUsersShortData();
};

const hashInput = input => sha3_256(input);

const isPresent = string => !validator.isEmpty(string) && validator.isAscii(string);

const isEmailNotExist = email => !(
  SHORT_USERS_DATA
    && SHORT_USERS_DATA.some(
      user => user.email.toUpperCase() === email.toUpperCase()
    )
);

export const validateEmail = email => isPresent(email) && validator.isEmail(email);

export const validateNewEmail = async email => {
  await updateUsersData();
  const hashedEmail = hashInput(email);
  return validateEmail(email) && isEmailNotExist(hashedEmail);
};

const isUsernameNotExist = username => !(
  SHORT_USERS_DATA
    && SHORT_USERS_DATA.some(
      user => user.username.toUpperCase() === username.toUpperCase()
    )
);

export const validateUsername = async username => {
  await updateUsersData();
  const hashedUsername = hashInput(username);
  return (
    isPresent(username)
    && validator.isLength(username, { min: 1, max: MAX_USERNAME_LENGTH })
    && isUsernameNotExist(hashedUsername)
  );
};

export const validatePassword = password => isPresent(password)
  && validator.isLength(password, { min: 4, max: MAX_PASSWORD_LENGTH });
