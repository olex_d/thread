import callWebApi from 'src/helpers/webApiHelper';
import { reaction } from './reactionsDictionary';

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const getAllCommentLikers = async request => {
  const response = await callWebApi({
    endpoint: `/api/comments/${request.id}/${request.from}/${request.count}`,
    type: 'GET'
  });
  return response.json();
};

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const editComment = async request => {
  const response = await callWebApi({
    endpoint: `/api/comments/${request.id}`,
    type: 'PUT',
    request
  });
  return response.json();
};

export const deleteComment = async request => {
  await callWebApi({
    endpoint: `/api/comments/${request.id}`,
    type: 'DELETE'
  });
};

export const reactToComment = async (commentId, commentReaction) => {
  const response = await callWebApi({
    endpoint: '/api/commentreaction',
    type: 'PUT',
    request: {
      commentId,
      isLike: commentReaction === reaction.LIKE
    }
  });
  return response.json();
};
