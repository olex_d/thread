export const reactiveEntityType = {
  POST: 'post',
  COMMENT: 'comment'
};

export const reaction = {
  LIKE: 'like',
  DISLIKE: 'dislike'
};
