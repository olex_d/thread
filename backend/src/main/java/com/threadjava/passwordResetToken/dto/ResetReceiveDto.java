package com.threadjava.passwordResetToken.dto;

import lombok.Data;

@Data
public class ResetReceiveDto {
	private String currentUrl;
	private String email;
}
