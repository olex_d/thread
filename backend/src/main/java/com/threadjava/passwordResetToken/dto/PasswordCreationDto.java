package com.threadjava.passwordResetToken.dto;

import lombok.Data;

@Data
public class PasswordCreationDto {
	private String passwordResetToken;
	private String newPassword;
}
