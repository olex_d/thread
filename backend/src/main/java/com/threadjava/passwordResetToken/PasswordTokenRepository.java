package com.threadjava.passwordResetToken;

import com.threadjava.passwordResetToken.model.PasswordResetToken;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PasswordTokenRepository extends CrudRepository<PasswordResetToken, UUID> {
	Optional<PasswordResetToken> findByToken(String token);
}
