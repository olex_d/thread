package com.threadjava.passwordResetToken;

import com.threadjava.passwordResetToken.model.PasswordResetToken;
import com.threadjava.users.model.User;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class PasswordTokenService {

	private static final byte HOURS_TO_EXPIRATION = 3;

	private final PasswordTokenRepository passwordTokenRepository;

	public PasswordTokenService(PasswordTokenRepository passwordTokenRepository) {
		this.passwordTokenRepository = passwordTokenRepository;
	}

	public void createPasswordResetTokenForUser(User user, String token) {
		var expirationDateTime = LocalDateTime.now().plusHours(HOURS_TO_EXPIRATION);
		var myToken = new PasswordResetToken(token, user, expirationDateTime);
		passwordTokenRepository.save(myToken);
	}

	public PasswordResetToken validateResetToken(String token) throws IllegalArgumentException {
		final var passToken = passwordTokenRepository.findByToken(token);
		if (passToken.isPresent() && !isTokenExpired(passToken.get())) return passToken.get();
		throw new IllegalArgumentException("Token invalid");
	}

	private boolean isTokenExpired(PasswordResetToken passToken) {
		return LocalDateTime.now().isAfter(passToken.getExpireDate());
	}

	public User getUserByPasswordResetToken(String token) {
		var tokenEntity = passwordTokenRepository.findByToken(token);
		if (tokenEntity.isEmpty()) throw new IllegalArgumentException("Token invalid");
		return tokenEntity.get().getUser();
	}

}
