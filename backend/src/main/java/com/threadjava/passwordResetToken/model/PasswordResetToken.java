package com.threadjava.passwordResetToken.model;

import com.threadjava.db.BaseEntity;
import com.threadjava.users.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "password_reset_tokens")
public class PasswordResetToken extends BaseEntity {

	@Column(name = "token")
	private String token;

	@OneToOne
	@JoinColumn(nullable = false, name = "user_id")
	private User user;

	@Column(name = "expire_date")
	private LocalDateTime expireDate;

}
