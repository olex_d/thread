package com.threadjava.passwordResetToken;

import com.threadjava.passwordResetToken.model.PasswordResetToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/passwordToken")
public class PasswordTokenController {

	private final PasswordTokenService passwordTokenService;

	public PasswordTokenController(PasswordTokenService passwordTokenService) {
		this.passwordTokenService = passwordTokenService;
	}

	@GetMapping
	public PasswordResetToken validate(@RequestParam String token) {
		return passwordTokenService.validateResetToken(token);
	}

}
