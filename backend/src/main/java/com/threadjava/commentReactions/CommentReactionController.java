package com.threadjava.commentReactions;

import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/commentreaction")
public class CommentReactionController {

	private final CommentReactionService commentReactionService;

	public CommentReactionController(CommentReactionService commentReactionService) {
		this.commentReactionService = commentReactionService;
	}

	@PutMapping
	public Optional<ResponseCommentReactionDto> setReaction(@RequestBody ReceivedCommentReactionDto commentReaction) {
		commentReaction.setUserId(getUserId());
		var reactionResult = commentReactionService.setReaction(commentReaction);

		reactionResult.ifPresent(commentReactionService::notifyCommentReaction);

		return reactionResult;
	}
}
