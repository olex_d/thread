package com.threadjava.commentReactions.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ResponseCommentReactionDto {
	private UUID id;
	private Boolean isLike;
	private UUID commentAuthorId;
	private UUID commentId;
	private UUID commentPostId;
	private UUID userId;
}
