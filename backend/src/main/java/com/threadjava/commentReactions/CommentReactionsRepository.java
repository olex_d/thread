package com.threadjava.commentReactions;

import com.threadjava.commentReactions.model.CommentReaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CommentReactionsRepository extends CrudRepository<CommentReaction, UUID> {
	@Query(value = "SELECT * FROM comment_reactions cr " +
			"WHERE cr.user_id = :userId AND cr.comment_id = :commentId ",
			nativeQuery = true)
	Optional<CommentReaction> getCommentReactionByUserIdAndCommentId(
			@Param("userId") UUID userId, @Param("commentId") UUID commentId);
}
