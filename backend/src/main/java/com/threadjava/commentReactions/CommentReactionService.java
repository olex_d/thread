package com.threadjava.commentReactions;

import com.threadjava.comment.CommentService;
import com.threadjava.commentReactions.dto.ReceivedCommentReactionDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import com.threadjava.notification.NotificationSender;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

import static com.threadjava.auth.TokenService.getUserId;

@Service
public class CommentReactionService {

	private final CommentReactionsRepository commentReactionsRepository;
	private final CommentService commentService;
	private final NotificationSender notificationSender;

	public CommentReactionService(CommentReactionsRepository commentReactionsRepository,
	                              CommentService commentService, NotificationSender notificationSender) {
		this.commentReactionsRepository = commentReactionsRepository;
		this.commentService = commentService;
		this.notificationSender = notificationSender;
	}

	public Optional<ResponseCommentReactionDto> setReaction(ReceivedCommentReactionDto commentReactionDto) {
		var reaction = commentReactionsRepository
				.getCommentReactionByUserIdAndCommentId(commentReactionDto.getUserId(), commentReactionDto.getCommentId());
		if (reaction.isPresent()) {
			var react = reaction.get();
			if (react.getIsActive()) {
				if (react.getIsLike() == commentReactionDto.getIsLike()) { // deleting existing reaction
					commentReactionsRepository.deleteById(react.getId());
					reaction.get().setIsLike(false);
				} else { // toggle from reaction type to another reaction type
					react.setIsLike(commentReactionDto.getIsLike());
					reaction = Optional.of(commentReactionsRepository.save(react));
				}
			} else { // reactivate existing reaction
				react.setIsLike(commentReactionDto.getIsLike());
				react.setIsActive(true);
				reaction = Optional.of(commentReactionsRepository.save(react));
			}
		} else { // adding new reaction
			var commentReaction = CommentReactionMapper.MAPPER.dtoToCommentReaction(commentReactionDto);
			reaction = Optional.of(commentReactionsRepository.save(commentReaction));
		}

		var response = Optional.of(CommentReactionMapper.MAPPER.reactionToCommentReactionDto(reaction.get()));

		// resolve authorId and postId for the first like/dislike
		if (Objects.isNull(response.get().getCommentAuthorId())
				|| Objects.isNull(response.get().getCommentPostId())) {
			var comment = commentService.getCommentById(commentReactionDto.getCommentId());
			var commentAuthorId = comment.getUser().getId();
			var commentPostId = comment.getPostId();
			response.get().setCommentAuthorId(commentAuthorId);
			response.get().setCommentPostId(commentPostId);
		}

		return response;
	}

	public void notifyCommentReaction(ResponseCommentReactionDto reaction) {
		notificationSender.notifyAboutCommentReaction(reaction);
	}

}
