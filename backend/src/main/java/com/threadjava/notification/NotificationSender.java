package com.threadjava.notification;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.commentReactions.dto.ResponseCommentReactionDto;
import com.threadjava.mail.MailSender;
import com.threadjava.mail.dto.MailContentDto;
import com.threadjava.post.dto.PostInteractionResponseDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class NotificationSender {

	private final static String NEW_POST_DESTINATION = "/topic/new_post";
	private final static String EDIT_POST_DESTINATION = "/topic/edit_post";
	private final static String DELETE_POST_DESTINATION = "/topic/delete_post";
	private final static String POST_REACTION_DESTINATION = "/topic/post_reaction";

	private final static String NEW_COMMENT_DESTINATION = "/topic/new_comment";
	private final static String EDIT_COMMENT_DESTINATION = "/topic/edit_comment";
	private final static String DELETE_COMMENT_DESTINATION = "/topic/delete_comment";
	private final static String COMMENT_LIKE_DESTINATION = "/topic/comment_reaction";

	private final SimpMessagingTemplate template;
	private final MailSender mailSender;

	public NotificationSender(SimpMessagingTemplate template, MailSender mailSender) {
		this.template = template;
		this.mailSender = mailSender;
	}

	@Async
	public void notifyAboutNewPost(PostInteractionResponseDto newPost) {
		template.convertAndSend(NEW_POST_DESTINATION, newPost);
	}

	@Async
	public void notifyAboutEditPost(PostInteractionResponseDto editPost) {
		template.convertAndSend(EDIT_POST_DESTINATION, editPost);
	}

	@Async
	public void notifyAboutDeletePost(UUID deletePostId) {
		template.convertAndSend(DELETE_POST_DESTINATION, deletePostId);
	}

	@Async
	public void notifyAboutPostReaction(ResponsePostReactionDto responseDto, MailContentDto mailContent) {
		if (mailContent != null && responseDto.getPostAuthorId() != responseDto.getUserId()) {
			mailSender.sendMessage(mailContent);
		}
		template.convertAndSend(POST_REACTION_DESTINATION, responseDto);
	}

	@Async
	public void notifyAboutNewComment(CommentDetailsDto responseDto) {
		template.convertAndSend(NEW_COMMENT_DESTINATION, responseDto);
	}

	@Async
	public void notifyAboutEditComment(CommentDetailsDto responseDto) {
		template.convertAndSend(EDIT_COMMENT_DESTINATION, responseDto);
	}

	@Async
	public void notifyAboutDeleteComment(UUID postId) {
		template.convertAndSend(DELETE_COMMENT_DESTINATION, postId);
	}

	@Async
	public void notifyAboutCommentReaction(ResponseCommentReactionDto responseDto) {
		template.convertAndSend(COMMENT_LIKE_DESTINATION, responseDto);
	}

}
