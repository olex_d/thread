package com.threadjava.auth;

import com.threadjava.auth.dto.AuthUserDto;
import com.threadjava.auth.dto.UserLoginDto;
import com.threadjava.auth.dto.UserRegisterDto;
import com.threadjava.passwordResetToken.dto.PasswordCreationDto;
import com.threadjava.passwordResetToken.dto.ResetReceiveDto;
import com.threadjava.users.dto.UserShortDto;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

	private final AuthService authService;

	public AuthController(AuthService authService) {
		this.authService = authService;
	}

	@PostMapping("/register")
	public AuthUserDto signUp(@RequestBody UserRegisterDto user) throws Exception {
		return authService.register(user);
	}

	@PostMapping("/login")
	public AuthUserDto login(@RequestBody UserLoginDto user) throws Exception {
		return authService.login(user);
	}

	@PostMapping("/resetPassword")
	public UserShortDto subscribeToPasswordReset(@RequestBody ResetReceiveDto resetDto) throws BadCredentialsException {
		return authService.subscribeToPasswordReset(resetDto);
	}

	@PostMapping("/createPassword")
	public UserShortDto createPassword(@RequestBody PasswordCreationDto creationDto) {
		return authService.createPassword(creationDto);
	}

}
