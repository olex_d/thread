package com.threadjava.auth;

import com.threadjava.auth.dto.AuthUserDto;
import com.threadjava.auth.dto.UserLoginDto;
import com.threadjava.auth.dto.UserRegisterDto;
import com.threadjava.auth.model.AuthUser;
import com.threadjava.mail.MailBuilder;
import com.threadjava.mail.MailSender;
import com.threadjava.passwordResetToken.PasswordTokenService;
import com.threadjava.passwordResetToken.dto.PasswordCreationDto;
import com.threadjava.passwordResetToken.dto.ResetReceiveDto;
import com.threadjava.users.UserMapper;
import com.threadjava.users.UsersService;
import com.threadjava.users.dto.UserShortDto;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.UUID;

@Service
public class AuthService {

	private final PasswordEncoder bCryptPasswordEncoder;
	private final AuthenticationManager authenticationManager;
	private final TokenService tokenService;
	private final UsersService userDetailsService;
	private final PasswordTokenService passwordTokenService;
	private final MailBuilder mailBuilder;
	private final MailSender mailSender;

	public AuthService(PasswordEncoder bCryptPasswordEncoder, AuthenticationManager authenticationManager,
	                   TokenService tokenService, UsersService userDetailsService,
	                   PasswordTokenService passwordTokenService, MailBuilder mailBuilder, MailSender mailSender) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.authenticationManager = authenticationManager;
		this.tokenService = tokenService;
		this.userDetailsService = userDetailsService;
		this.passwordTokenService = passwordTokenService;
		this.mailBuilder = mailBuilder;
		this.mailSender = mailSender;
	}

	public AuthUserDto register(UserRegisterDto userDto) throws Exception {
		var user = AuthUserMapper.MAPPER.userRegisterDtoToUser(userDto);
		var loginDTO = new UserLoginDto(user.getEmail(), user.getPassword());
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userDetailsService.save(user);
		return login(loginDTO);
	}

	public AuthUserDto login(UserLoginDto user) throws Exception {
		Authentication auth;
		try {
			auth = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
		} catch (BadCredentialsException e) {
			throw new Exception("Incorrect username or password", e);
		}

		var currentUser = (AuthUser) auth.getPrincipal();
		final var userDetails = userDetailsService.getUserById(currentUser.getId());
		final String jwt = tokenService.generateToken(currentUser);
		return new AuthUserDto(jwt, userDetails);
	}

	public UserShortDto subscribeToPasswordReset(ResetReceiveDto resetDto) throws BadCredentialsException {
		var authUser = userDetailsService.loadUserByUsername(resetDto.getEmail());
		if (Objects.isNull(authUser)) throw new BadCredentialsException("Incorrect verification email");

		var userDetails = userDetailsService.getUserById(authUser.getId());
		var user = UserMapper.MAPPER.userDetailsDtoToUser(userDetails);

		final var resetToken = UUID.randomUUID().toString();
		passwordTokenService.createPasswordResetTokenForUser(user, resetToken);
		final var resetContent = mailBuilder.buildResetMessage(resetToken, userDetails, resetDto.getCurrentUrl());
		mailSender.sendMessage(resetContent);

		return UserMapper.MAPPER.userToUserShortDto(user);
	}

	public UserShortDto createPassword(PasswordCreationDto creationDto) {
		var user = passwordTokenService.getUserByPasswordResetToken(creationDto.getPasswordResetToken());
		user.setPassword(bCryptPasswordEncoder.encode(creationDto.getNewPassword()));
		userDetailsService.save(user);
		return UserMapper.MAPPER.userToUserShortDto(user);
	}

}
