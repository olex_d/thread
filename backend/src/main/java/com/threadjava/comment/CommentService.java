package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentStoringDto;
import com.threadjava.comment.model.Comment;
import com.threadjava.notification.NotificationSender;
import com.threadjava.post.PostMapper;
import com.threadjava.post.PostsService;
import com.threadjava.post.dto.PostCommentDto;
import com.threadjava.post.dto.PostUserDto;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CommentService {

	private final CommentRepository commentRepository;
	private final PostsService postsService;
	private final NotificationSender notificationSender;

	public CommentService(CommentRepository commentRepository, PostsService postsService,
	                      NotificationSender notificationSender) {
		this.commentRepository = commentRepository;
		this.postsService = postsService;
		this.notificationSender = notificationSender;
	}

	public PostCommentDto getCommentById(UUID id) {
		return commentRepository.findCommentById(id)
				.map(PostMapper.MAPPER::commentQueryToPostCommentDto)
				.orElseThrow();
	}

	public List<PostUserDto> getCommentLikers(Integer from, Integer count, UUID commentId) {
		var pageable = PageRequest.of(from / count, count);
		return commentRepository
				.findUsersByLikedComment(commentId, pageable)
				.stream()
				.map(PostMapper.MAPPER::userToPostUserDto)
				.collect(Collectors.toList());
	}

	public CommentDetailsDto create(CommentStoringDto commentDto) {
		Comment comment = CommentMapper.MAPPER.commentInteractionDtoToModel(commentDto);
		var commentCreated = commentRepository.save(comment);

		var creationDetails = CommentMapper.MAPPER.commentToCommentDetailsDto(commentCreated);
		if (Objects.isNull(creationDetails.getPostAuthorId())) { // resolve authorId
			var post = postsService.getPostById(creationDetails.getPostId());
			var postAuthorId = post.getUser().getId();
			creationDetails.setPostAuthorId(postAuthorId);
		}

		notificationSender.notifyAboutNewComment(creationDetails);

		return creationDetails;
	}

	public PostCommentDto update(UUID commentId, CommentStoringDto updateDTO) {
		Comment comment = CommentMapper.MAPPER.commentInteractionDtoToModel(updateDTO);
		comment.setId(commentId);
		var commentUpdated = commentRepository.save(comment);

		var updatingDetails = CommentMapper.MAPPER.commentToCommentDetailsDto(commentUpdated);
		notificationSender.notifyAboutEditComment(updatingDetails);

		return getCommentById(commentId);
	}

	public void delete(UUID commentId) {
		var postId  = getCommentById(commentId).getPostId();
		commentRepository.deleteById(commentId);
		notificationSender.notifyAboutDeleteComment(postId);
	}

}
