package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.comment.model.Comment;
import com.threadjava.users.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CommentRepository extends JpaRepository<Comment, UUID> {

	@Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(c.id, c.body, " +
			"(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
			"(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
			"c.createdAt, c.post, c.user) " +
			"FROM Comment c " +
			"WHERE c.id = :commentId")
	Optional<CommentDetailsQueryResult> findCommentById(@Param("commentId") UUID commentId);

	@Query("SELECT new com.threadjava.comment.dto.CommentDetailsQueryResult(c.id, c.body, " +
			"(SELECT COALESCE(SUM(CASE WHEN cr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
			"(SELECT COALESCE(SUM(CASE WHEN cr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM c.reactions cr WHERE cr.comment = c), " +
			"c.createdAt, c.post, c.user) " +
			"FROM Comment c " +
			"WHERE c.post.id = :postId")
	List<CommentDetailsQueryResult> findAllByPostId(@Param("postId") UUID postId);

	@Query("SELECT distinct u FROM User u " +
			"JOIN CommentReaction cr ON u.id = cr.user.id " +
			"WHERE cr.comment.id = :commentId AND cr.isLike = true AND cr.isActive = true")
	List<User> findUsersByLikedComment(@Param("commentId") UUID commentId, Pageable pageable);

}
