package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentStoringDto;
import com.threadjava.post.dto.PostCommentDto;
import com.threadjava.post.dto.PostUserDto;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/comments")
public class CommentController {

	private final CommentService commentService;

	public CommentController(CommentService commentService, SimpMessagingTemplate template) {
		this.commentService = commentService;
	}

	@GetMapping("/{id}")
	public PostCommentDto get(@PathVariable UUID id) {
		return commentService.getCommentById(id);
	}

	@GetMapping("/{id}/{from}/{count}")
	public List<PostUserDto> getLikers(@RequestParam(defaultValue = "0") Integer from,
	                                   @RequestParam(defaultValue = "10") Integer count,
	                                   @PathVariable("id") UUID commentId) {
		return commentService.getCommentLikers(from, count, commentId);
	}

	@PostMapping
	public CommentDetailsDto post(@RequestBody CommentStoringDto commentDto) {
		commentDto.setUserId(getUserId());
		return commentService.create(commentDto);
	}

	@PutMapping("/{id}")
	public PostCommentDto update(@PathVariable UUID id, @RequestBody CommentStoringDto putDto) {
		putDto.setUserId(getUserId());
		return commentService.update(id, putDto);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable UUID id) {
		commentService.delete(id);
	}

}
