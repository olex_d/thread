package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentStoringDto;
import com.threadjava.comment.model.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommentMapper {

	CommentMapper MAPPER = Mappers.getMapper(CommentMapper.class);

	@Mapping(source = "post.id", target = "postId")
	@Mapping(source = "post.user.id", target = "postAuthorId")
	CommentDetailsDto commentToCommentDetailsDto(Comment comment);

	@Mapping(source = "postId", target = "post.id")
	@Mapping(source = "userId", target = "user.id")
	@Mapping(target = "id", ignore = true)
	@Mapping(target = "createdAt", ignore = true)
	@Mapping(target = "updatedAt", ignore = true)
	@Mapping(target = "isActive", ignore = true)
	@Mapping(target = "reactions", ignore = true)
	Comment commentInteractionDtoToModel(CommentStoringDto commentDto);

}
