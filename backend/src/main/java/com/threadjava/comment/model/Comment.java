package com.threadjava.comment.model;

import com.threadjava.commentReactions.model.CommentReaction;
import com.threadjava.db.BaseEntity;
import com.threadjava.post.model.Post;
import com.threadjava.postReactions.model.PostReaction;
import com.threadjava.users.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@SQLDelete(sql = "UPDATE comments SET is_active = false WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "is_active <> false")
@Entity
@Table(name = "comments")
public class Comment extends BaseEntity {

	@Column(name = "body", columnDefinition = "TEXT")
	private String body;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "user_id")
	private User user;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "post_id")
	private Post post;

	@OneToMany(mappedBy = "comment", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<CommentReaction> reactions = new ArrayList<>();

	@Column(name = "is_active")
	private Boolean isActive = true;

}
