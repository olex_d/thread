package com.threadjava.comment.dto;

import com.threadjava.post.model.Post;
import com.threadjava.users.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentDetailsQueryResult {
	private UUID id;
	private String body;
	public long likeCount;
	public long dislikeCount;
	private Date createdAt;
	private Post post;
	private User user;
}
