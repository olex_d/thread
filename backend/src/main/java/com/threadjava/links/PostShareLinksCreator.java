package com.threadjava.links;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.UUID;

@Service
public class PostShareLinksCreator {

	@Value("${server.port}")
	private String SPRING_BOOT_PORT;
	@Value("${client.port}")
	private String CLIENT_PORT;

	public String create(UUID postId) {
		var baseUrl = ServletUriComponentsBuilder.fromCurrentContextPath()
				.replacePath(null)
				.build()
				.toUriString()
				.replace(SPRING_BOOT_PORT, CLIENT_PORT);
		return baseUrl + "/share/" + postId;
	}

}
