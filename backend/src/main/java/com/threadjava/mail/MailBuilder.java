package com.threadjava.mail;

import com.threadjava.mail.dto.MailContentDto;
import com.threadjava.post.dto.PostEmailDto;
import com.threadjava.users.dto.UserDetailsDto;
import org.springframework.stereotype.Service;

@Service
public class MailBuilder {

	public MailContentDto buildResetMessage(String resetToken, UserDetailsDto resetUser, String currentUrl) {
		final var resetSubject = "Thread App Password Reset";
		final var resetText = String.format(
			"Hello, %s!\n" +
			"We have received your password resetting ticket recently!\n" +
			"If it is related, in next six hours go to: %s/resetPassword/%s\n" +
			"Otherwise, just ignore this message.\n" +
			"Best regards, Thread App Team!",
			resetUser.getUsername(), currentUrl, resetToken
		);
		return new MailContentDto(resetSubject, resetText, resetUser.getEmail());
	}

	public MailContentDto buildSharingMessage(PostEmailDto shareDto) {
		final var shareSubject = "Thread Post Sharing";
		final var shareText = String.format(
			"Hello!\n" +
			"%s has just shared the post with you!\n" +
			"Have a look: %s",
			shareDto.getSender().getUsername(), shareDto.getPostLink()
		);
		return new MailContentDto(shareSubject, shareText, shareDto.getRecipientEmail());
	}

	public MailContentDto buildLikeMessage(PostEmailDto likeDto) {
		final var likeSubject = "Thread Post Liked";
		final var likeText = String.format(
			"Hello!\n" +
			"%s has just liked your post!\n" +
			"Post: %s",
			likeDto.getSender().getUsername(), likeDto.getPostLink()
		);
		return new MailContentDto(likeSubject, likeText, likeDto.getRecipientEmail());
	}

}
