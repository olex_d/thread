package com.threadjava.mail.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MailContentDto {
	private String messageSubject;
	private String messageBody;
	private String recipientEmail;
}
