package com.threadjava.mail;

import com.threadjava.mail.dto.MailContentDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class MailSender {

	private final JavaMailSender mailSender;

	public MailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	@Value("${spring.mail.username}")
	private String username;

	@Async
	public void sendMessage(MailContentDto content) {
		var mailMessage = new SimpleMailMessage();
		mailMessage.setFrom(username);
		mailMessage.setTo(content.getRecipientEmail());
		mailMessage.setSubject(content.getMessageSubject());
		mailMessage.setText(content.getMessageBody());
		mailSender.send(mailMessage);
	}

}
