package com.threadjava.post;

import com.threadjava.comment.dto.CommentDetailsQueryResult;
import com.threadjava.image.ImageMapper;
import com.threadjava.post.dto.PostCommentDto;
import com.threadjava.post.dto.PostDetailsDto;
import com.threadjava.post.dto.PostDetailsQueryResult;
import com.threadjava.post.dto.PostInteractionResponseDto;
import com.threadjava.post.dto.PostListDto;
import com.threadjava.post.dto.PostListQueryResult;
import com.threadjava.post.dto.PostStoringDto;
import com.threadjava.post.dto.PostUserDto;
import com.threadjava.post.model.Post;
import com.threadjava.users.model.User;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {ImageMapper.class})
public abstract class PostMapper {

	public static final PostMapper MAPPER = Mappers.getMapper(PostMapper.class);

	@Mapping(target = "comments", ignore = true)
	public abstract PostDetailsDto postToPostDetailsDto(PostDetailsQueryResult post);

	@Mapping(source = "post.id", target = "postId")
	public abstract PostCommentDto commentQueryToPostCommentDto(CommentDetailsQueryResult comment);

	@Mapping(source = "user.id", target = "userId")
	public abstract PostInteractionResponseDto postToPostInteractionResponseDto(Post post);

	@Mapping(source = "imageId", target = "image.id",
			nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
	@Mapping(source = "userId", target = "user.id")
	@Mapping(target = "id", ignore = true)
	@Mapping(target = "createdAt", ignore = true)
	@Mapping(target = "updatedAt", ignore = true)
	@Mapping(target = "comments", ignore = true)
	@Mapping(target = "reactions", ignore = true)
	@Mapping(target = "isActive", ignore = true)
	public abstract Post postStoringDtoToPost(PostStoringDto postDetailsDto);

	@AfterMapping
	public Post doAfterMapping(@MappingTarget Post entity) {
		if (entity != null && entity.getImage().getId() == null) {
			entity.setImage(null);
		}
		return entity;
	}

	public abstract PostListDto postListToPostListDto(PostListQueryResult model);

	@Mapping(source = "avatar", target = "image")
	public abstract PostUserDto userToPostUserDto(User model);

}
