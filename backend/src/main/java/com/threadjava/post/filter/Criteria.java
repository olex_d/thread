package com.threadjava.post.filter;

public enum Criteria {
	UserPosts,
	NotUserPosts,
	UserLikedPosts
}
