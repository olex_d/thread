package com.threadjava.post.filter;

import com.threadjava.post.PostsRepository;
import com.threadjava.post.dto.PostFilterDTO;
import com.threadjava.post.dto.PostListQueryResult;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Objects;
import java.util.stream.Stream;

@Service
public class PostsFilterService {

	private final PostsRepository postsRepository;

	public PostsFilterService(PostsRepository postsRepository) {
		this.postsRepository = postsRepository;
	}

	public Stream<PostListQueryResult> filter(PostFilterDTO filter, EntityManager manager) {
		var pageable = PageRequest.of(filter.getFrom() / filter.getCount(), filter.getCount());
		if (Objects.nonNull(filter.getCriterion())) {
			var filterQuery = createFilterQuery(filter);
			return manager.createQuery(filterQuery, PostListQueryResult.class)
					.setParameter("userId", filter.getUserId())
					.setFirstResult(filter.getFrom())
					.setMaxResults(filter.getCount())
					.getResultStream();
		}
		return postsRepository
				.findAllPosts(pageable)
				.stream();
	}

	private String createFilterQuery(PostFilterDTO filter) {
		final var query = new StringBuilder(
				"SELECT DISTINCT new com.threadjava.post.dto.PostListQueryResult(p.id, p.body, " +
						"(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
						"(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
						"(SELECT COUNT(*) FROM p.comments), " +
						"p.createdAt, i, p.user) " +
						"FROM Post p " +
						"LEFT JOIN p.image i "
		);
		var whereStatement = new StringBuilder();

		if (filter.getCriterion().contains(Criteria.UserPosts.name())) {
			appendCondition(whereStatement, " p.user.id = :userId ");
		}
		if (filter.getCriterion().contains(Criteria.NotUserPosts.name())) {
			appendCondition(whereStatement, " p.user.id != :userId ");
		}
		if (filter.getCriterion().contains(Criteria.UserLikedPosts.name())) {
			query.append(" JOIN p.reactions pr ");
			appendCondition(whereStatement, " pr.user.id = :userId AND pr.isLike = true ");
		}
		whereStatement.insert(0, " WHERE ");

		query.append(whereStatement)
				.append(" ORDER BY p.createdAt DESC ");

		return query.toString();
	}

	private void appendCondition(StringBuilder statement, String condition) {
		if (statement.length() > 0) statement.append(" AND ");
		statement.append(condition);
	}

}
