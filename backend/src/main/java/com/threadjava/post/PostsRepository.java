package com.threadjava.post;

import com.threadjava.post.dto.PostDetailsQueryResult;
import com.threadjava.post.dto.PostListQueryResult;
import com.threadjava.post.model.Post;
import com.threadjava.users.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PostsRepository extends JpaRepository<Post, UUID> {

	@Query("SELECT new com.threadjava.post.dto.PostListQueryResult(p.id, p.body, " +
			"(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
			"(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
			"(SELECT COUNT(*) FROM p.comments), " +
			"p.createdAt, i, p.user) " +
			"FROM Post p " +
			"LEFT JOIN p.image i " +
			"order by p.createdAt desc")
	List<PostListQueryResult> findAllPosts(Pageable pageable);

	@Query("SELECT new com.threadjava.post.dto.PostDetailsQueryResult(p.id, p.body, " +
			"(SELECT COALESCE(SUM(CASE WHEN pr.isLike = TRUE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
			"(SELECT COALESCE(SUM(CASE WHEN pr.isLike = FALSE THEN 1 ELSE 0 END), 0) FROM p.reactions pr WHERE pr.post = p), " +
			"(SELECT COUNT(*) FROM p.comments), " +
			"p.createdAt, p.updatedAt, i, p.user) " +
			"FROM Post p " +
			"LEFT JOIN p.image i " +
			"WHERE p.id = :id")
	Optional<PostDetailsQueryResult> findPostById(@Param("id") UUID id);

	@Query("SELECT distinct u FROM User u " +
			"JOIN PostReaction pr ON u.id = pr.user.id " +
			"WHERE pr.post.id = :postId AND pr.isLike = true AND pr.isActive = true")
	List<User> findUsersByLikedPost(@Param("postId") UUID postId, Pageable pageable);

}
