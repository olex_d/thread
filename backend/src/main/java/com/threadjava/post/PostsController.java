package com.threadjava.post;

import com.threadjava.post.dto.PostDetailsDto;
import com.threadjava.post.dto.PostEmailDto;
import com.threadjava.post.dto.PostFilterDTO;
import com.threadjava.post.dto.PostInteractionResponseDto;
import com.threadjava.post.dto.PostListDto;
import com.threadjava.post.dto.PostStoringDto;
import com.threadjava.post.dto.PostUserDto;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {

	private final PostsService postsService;

	public PostsController(PostsService postsService, SimpMessagingTemplate template) {
		this.postsService = postsService;
	}

	@GetMapping
	public List<PostListDto> getAll(PostFilterDTO filter) {
		return postsService.getAllPosts(filter);
	}

	@GetMapping("/{id}")
	public PostDetailsDto getPost(@PathVariable("id") UUID postId) {
		return postsService.getPostById(postId);
	}

	@GetMapping("/{id}/{from}/{count}")
	public List<PostUserDto> getLikers(@RequestParam(defaultValue = "0") Integer from,
	                                   @RequestParam(defaultValue = "10") Integer count,
	                                   @PathVariable("id") UUID postId) {
		return postsService.getPostLikers(from, count, postId);
	}

	@PostMapping
	public PostInteractionResponseDto post(@RequestBody PostStoringDto postDto) {
		postDto.setUserId(getUserId());
		return postsService.create(postDto);
	}

	@PutMapping("/{id}")
	public PostInteractionResponseDto update(@PathVariable UUID id, @RequestBody PostStoringDto putDto) {
		putDto.setUserId(getUserId());
		return postsService.update(id, putDto);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable UUID id) {
		postsService.delete(id);
	}

	@PostMapping("/shareByEmail")
	public void sharePost(@RequestBody PostEmailDto shareDto) {
		postsService.sharePostByEmail(shareDto);
	}

}
