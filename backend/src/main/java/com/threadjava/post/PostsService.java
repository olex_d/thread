package com.threadjava.post;

import com.threadjava.comment.CommentRepository;
import com.threadjava.mail.MailBuilder;
import com.threadjava.mail.MailSender;
import com.threadjava.notification.NotificationSender;
import com.threadjava.post.dto.PostDetailsDto;
import com.threadjava.post.dto.PostEmailDto;
import com.threadjava.post.dto.PostFilterDTO;
import com.threadjava.post.dto.PostInteractionResponseDto;
import com.threadjava.post.dto.PostListDto;
import com.threadjava.post.dto.PostStoringDto;
import com.threadjava.post.dto.PostUserDto;
import com.threadjava.post.filter.PostsFilterService;
import com.threadjava.users.UsersService;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostsService {

	@PersistenceUnit
	private final EntityManagerFactory entityManagerFactory;
	private final PostsRepository postsCrudRepository;
	private final CommentRepository commentRepository;
	private final PostsFilterService postsFilterService;
	private final UsersService usersService;
	private final NotificationSender notificationSender;
	private final MailBuilder mailBuilder;
	private final MailSender mailSender;

	public PostsService(EntityManagerFactory entityManagerFactory, PostsRepository postsCrudRepository,
	                    CommentRepository commentRepository, PostsFilterService postsFilterService,
	                    UsersService usersService, NotificationSender notificationSender, MailBuilder mailBuilder,
	                    MailSender mailSender) {
		this.entityManagerFactory = entityManagerFactory;
		this.postsCrudRepository = postsCrudRepository;
		this.commentRepository = commentRepository;
		this.postsFilterService = postsFilterService;
		this.usersService = usersService;
		this.notificationSender = notificationSender;
		this.mailBuilder = mailBuilder;
		this.mailSender = mailSender;
	}

	public List<PostListDto> getAllPosts(PostFilterDTO filter) {
		var manager = entityManagerFactory.createEntityManager();
		try {
			var posts = postsFilterService.filter(filter, manager);
			return posts
					.map(PostMapper.MAPPER::postListToPostListDto)
					.collect(Collectors.toList());
		} finally {
			if (manager.isOpen()) manager.close();
		}
	}

	public PostDetailsDto getPostById(UUID id) {
		var post = postsCrudRepository.findPostById(id)
				.map(PostMapper.MAPPER::postToPostDetailsDto)
				.orElseThrow();

		var comments = commentRepository.findAllByPostId(id)
				.stream()
				.map(PostMapper.MAPPER::commentQueryToPostCommentDto)
				.collect(Collectors.toList());
		post.setComments(comments);

		return post;
	}

	public List<PostUserDto> getPostLikers(Integer from, Integer count, UUID postId) {
		var pageable = PageRequest.of(from / count, count);
		return postsCrudRepository
				.findUsersByLikedPost(postId, pageable)
				.stream()
				.map(PostMapper.MAPPER::userToPostUserDto)
				.collect(Collectors.toList());
	}

	public PostInteractionResponseDto create(PostStoringDto postDto) {
		var post = PostMapper.MAPPER.postStoringDtoToPost(postDto);
		var postCreated = postsCrudRepository.save(post);

		var creationResponse = PostMapper.MAPPER.postToPostInteractionResponseDto(postCreated);
		notificationSender.notifyAboutNewPost(creationResponse);

		return creationResponse;
	}

	public PostInteractionResponseDto update(UUID postId, PostStoringDto updateDTO) {
		var post = PostMapper.MAPPER.postStoringDtoToPost(updateDTO);
		post.setId(postId);
		var postUpdated = postsCrudRepository.save(post);

		var editResponse = PostMapper.MAPPER.postToPostInteractionResponseDto(postUpdated);
		notificationSender.notifyAboutEditPost(editResponse);

		return editResponse;
	}

	public void delete(UUID postId) {
		postsCrudRepository.deleteById(postId);
		notificationSender.notifyAboutDeletePost(postId);
	}

	public void sharePostByEmail(PostEmailDto shareDto) {
		final var sender = usersService.getUserById(shareDto.getSender().getId());
		if (sender == null) throw new IllegalArgumentException("Sender is not registered in system");

		final var sharingContent = mailBuilder.buildSharingMessage(shareDto);
		mailSender.sendMessage(sharingContent);
	}

}
