package com.threadjava.post.dto;

import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class PostCommentDto {
	private UUID id;
	private UUID postId;
	private String body;
	private Date createdAt;
	public long likeCount;
	public long dislikeCount;
	private PostUserDto user;
}
