package com.threadjava.post.dto;

import com.threadjava.users.dto.UserShortDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostEmailDto {
	private UserShortDto sender;
	private String postLink;
	private String recipientEmail;
}
