package com.threadjava.post.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class PostInteractionResponseDto {
	private UUID id;
	private UUID userId;
}
