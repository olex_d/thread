package com.threadjava.post.dto;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class PostFilterDTO {
	private Integer from = 0;
	private Integer count = 10;
	private UUID userId;
	private List<String> criterion;
}
