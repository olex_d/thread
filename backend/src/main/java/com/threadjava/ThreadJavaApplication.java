package com.threadjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@SpringBootApplication
@EnableAsync
public class ThreadJavaApplication {

	private final static short CORE_POOL_SIZE = 4;
	private final static short MAX_POOL_SIZE = 4;
	private final static short QUEUE_CAPACITY = 500;
	private final static String THREAD_NAME = "ThreadApp-";

	@Bean
	public Executor taskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(CORE_POOL_SIZE);
		executor.setMaxPoolSize(MAX_POOL_SIZE);
		executor.setQueueCapacity(QUEUE_CAPACITY);
		executor.setThreadNamePrefix(THREAD_NAME);
		executor.initialize();
		return executor;
	}

	public static void main(String[] args) {
		SpringApplication.run(ThreadJavaApplication.class, args);
	}
}
