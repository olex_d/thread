package com.threadjava.postReactions;

import com.threadjava.links.PostShareLinksCreator;
import com.threadjava.mail.MailBuilder;
import com.threadjava.mail.dto.MailContentDto;
import com.threadjava.notification.NotificationSender;
import com.threadjava.post.PostsService;
import com.threadjava.post.dto.PostEmailDto;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import com.threadjava.users.UserMapper;
import com.threadjava.users.UsersService;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

import static com.threadjava.auth.TokenService.getUserId;

@Service
public class PostReactionService {

	private final PostReactionsRepository postReactionsRepository;
	private final PostsService postsService;
	private final UsersService usersService;
	private final MailBuilder mailBuilder;
	private final NotificationSender notificationSender;
	private final PostShareLinksCreator shareLinksCreator;

	public PostReactionService(PostReactionsRepository postReactionsRepository, PostsService postsService,
	                           UsersService usersService, MailBuilder mailBuilder,
	                           NotificationSender notificationSender, PostShareLinksCreator shareLinksCreator) {
		this.postReactionsRepository = postReactionsRepository;
		this.postsService = postsService;
		this.usersService = usersService;
		this.mailBuilder = mailBuilder;
		this.notificationSender = notificationSender;
		this.shareLinksCreator = shareLinksCreator;
	}

	public Optional<ResponsePostReactionDto> setReaction(ReceivedPostReactionDto postReactionDto) {
		var reaction = postReactionsRepository
				.getPostReactionByUserIdAndPostId(postReactionDto.getUserId(), postReactionDto.getPostId());
		if (reaction.isPresent()) {
			var react = reaction.get();
			if (react.getIsActive()) {
				if (react.getIsLike() == postReactionDto.getIsLike()) { // deleting existing reaction
					postReactionsRepository.deleteById(react.getId());
					reaction.get().setIsLike(false);
				} else { // toggle from reaction type to another reaction type
					react.setIsLike(postReactionDto.getIsLike());
					reaction = Optional.of(postReactionsRepository.save(react));
				}
			} else { // reactivate existing reaction
				react.setIsLike(postReactionDto.getIsLike());
				react.setIsActive(true);
				reaction = Optional.of(postReactionsRepository.save(react));
			}
		} else { // adding new reaction
			var postReaction = PostReactionMapper.MAPPER.dtoToPostReaction(postReactionDto);
			reaction = Optional.of(postReactionsRepository.save(postReaction));
		}

		var response = Optional.of(PostReactionMapper.MAPPER.reactionToResponseDto(reaction.get()));

		if (Objects.isNull(response.get().getPostAuthorId())) { // resolve authorId for the first like/dislike
			var post = postsService.getPostById(postReactionDto.getPostId());
			var postAuthorId = post.getUser().getId();
			response.get().setPostAuthorId(postAuthorId);
		}

		return response;
	}

	public void notifyPostReaction(ResponsePostReactionDto reaction) {
		MailContentDto likeMail = null;
		if (reaction.getIsLike()) {
			var post = postsService.getPostById(reaction.getPostId());
			var author = post.getUser();
			var currentUser = UserMapper.MAPPER.userDetailsDtoToUserShortDto(usersService.getUserById(getUserId()));

			var postLink = shareLinksCreator.create(post.getId());
			var likeMailContent = new PostEmailDto(currentUser, postLink, author.getEmail());
			likeMail = mailBuilder.buildLikeMessage(likeMailContent);
		}
		notificationSender.notifyAboutPostReaction(reaction, likeMail);
	}

}
