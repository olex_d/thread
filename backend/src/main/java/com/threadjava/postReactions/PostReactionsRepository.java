package com.threadjava.postReactions;

import com.threadjava.postReactions.model.PostReaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PostReactionsRepository extends CrudRepository<PostReaction, UUID> {

	@Query(value = "SELECT * FROM post_reactions pr " +
			"WHERE pr.user_id = :userId AND pr.post_id = :postId ",
			nativeQuery = true)
	Optional<PostReaction> getPostReactionByUserIdAndPostId(@Param("userId") UUID userId, @Param("postId") UUID postId);

}
