package com.threadjava.postReactions;

import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/postreaction")
public class PostReactionController {

	private final PostReactionService postsReactionsService;

	public PostReactionController(PostReactionService postsReactionsService) {
		this.postsReactionsService = postsReactionsService;
	}

	@PutMapping
	public Optional<ResponsePostReactionDto> setReaction(@RequestBody ReceivedPostReactionDto postReaction) {
		postReaction.setUserId(getUserId());
		var response = postsReactionsService.setReaction(postReaction);

		response.ifPresent(postsReactionsService::notifyPostReaction);

		return response;
	}

}
