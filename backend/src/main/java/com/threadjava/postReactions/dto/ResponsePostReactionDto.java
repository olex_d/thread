package com.threadjava.postReactions.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ResponsePostReactionDto {
	private UUID id;
	private Boolean isLike;
	private UUID postAuthorId;
	private UUID postId;
	private UUID userId;
}
