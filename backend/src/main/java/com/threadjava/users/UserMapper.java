package com.threadjava.users;

import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.dto.UserShortDto;
import com.threadjava.users.dto.UserStoringDto;
import com.threadjava.users.model.User;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class UserMapper {

	public static final UserMapper MAPPER = Mappers.getMapper(UserMapper.class);

	@Mapping(source = "avatar", target = "image")
	public abstract UserDetailsDto userToUserDetailsDto(User user);

	@Mapping(source = "image", target = "avatar")
	@Mapping(target = "password", ignore = true)
	@Mapping(target = "createdAt", ignore = true)
	@Mapping(target = "updatedAt", ignore = true)
	public abstract User userDetailsDtoToUser(UserDetailsDto userDetails);

	public abstract UserShortDto userToUserShortDto(User user);

	public abstract UserShortDto userDetailsDtoToUserShortDto(UserDetailsDto user);

	@Mapping(source = "imageId", target = "avatar.id",
			nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
	@Mapping(source = "username", target = "username",
			nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	@Mapping(source = "email", target = "email",
			nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	@Mapping(source = "status", target = "status",
			nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	@Mapping(target = "id", ignore = true)
	@Mapping(target = "password", ignore = true)
	@Mapping(target = "createdAt", ignore = true)
	@Mapping(target = "updatedAt", ignore = true)
	public abstract User userStoringDtoToUser(UserStoringDto storingDto);

	@AfterMapping
	public User doAfterMapping(@MappingTarget User entity) {
		if (entity != null && entity.getAvatar() != null && entity.getAvatar().getId() == null) {
			entity.setAvatar(null);
		}
		return entity;
	}
}
