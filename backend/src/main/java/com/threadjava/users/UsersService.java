package com.threadjava.users;

import com.threadjava.auth.model.AuthUser;
import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.dto.UserShortDto;
import com.threadjava.users.dto.UserStoringDto;
import com.threadjava.users.model.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA3_256;

@Service
public class UsersService implements UserDetailsService {

	private final UsersRepository usersRepository;

	public UsersService(UsersRepository usersRepository) {
		this.usersRepository = usersRepository;
	}

	@Override
	public AuthUser loadUserByUsername(String email) throws UsernameNotFoundException {
		return usersRepository
				.findByEmail(email)
				.map(user -> new AuthUser(user.getId(), user.getEmail(), user.getPassword()))
				.orElseThrow(() -> new UsernameNotFoundException(email));
	}

	public UserDetailsDto getUserById(UUID id) {
		return usersRepository
				.findById(id)
				.map(UserMapper.MAPPER::userToUserDetailsDto)
				.orElseThrow(() -> new UsernameNotFoundException("No user found with username"));
	}

	public List<UserShortDto> getUsersShortData() {
		final var hasher = new DigestUtils(SHA3_256);
		var usersData = usersRepository
				.findAll()
				.stream()
				.map(UserMapper.MAPPER::userToUserShortDto)
				.collect(Collectors.toList());
		usersData.forEach(shortData -> {
			shortData.setUsername(hasher.digestAsHex(shortData.getUsername()));
			shortData.setEmail(hasher.digestAsHex(shortData.getEmail()));
		});
		return usersData;
	}

	public void save(User user) {
		usersRepository.save(user);
	}

	public UserDetailsDto update(UUID userId, UserStoringDto updateDTO) {
		var user = usersRepository.findById(userId);
		if (user.isEmpty()) throw new IllegalArgumentException();
		User updatingUser = user.get();

		User userFromDto = UserMapper.MAPPER.userStoringDtoToUser(updateDTO);
		if (Objects.nonNull(userFromDto.getUsername())) updatingUser.setUsername(userFromDto.getUsername());
		if (Objects.nonNull(userFromDto.getEmail())) updatingUser.setEmail(userFromDto.getEmail());
		if (Objects.nonNull(userFromDto.getAvatar())) updatingUser.setAvatar(userFromDto.getAvatar());
		if (Objects.nonNull(userFromDto.getStatus())) updatingUser.setStatus(userFromDto.getStatus());

		updatingUser = usersRepository.save(updatingUser);
		return UserMapper.MAPPER.userToUserDetailsDto(updatingUser);
	}

}
