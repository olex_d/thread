package com.threadjava.users;

import com.threadjava.users.dto.UserDetailsDto;
import com.threadjava.users.dto.UserShortDto;
import com.threadjava.users.dto.UserStoringDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/user")
public class UserController {

	private final UsersService userDetailsService;

	public UserController(UsersService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	@GetMapping
	public UserDetailsDto getUser() {
		return userDetailsService.getUserById(getUserId());
	}

	@GetMapping("/shortdata")
	public List<UserShortDto> getUsersShortData() {
		return userDetailsService.getUsersShortData();
	}

	@PutMapping("/{id}")
	public UserDetailsDto update(@PathVariable UUID id, @RequestBody UserStoringDto putDto) {
		return userDetailsService.update(id, putDto);
	}

}
