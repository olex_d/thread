package com.threadjava.users.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class UserStoringDto {
	private UUID imageId;
	private String username;
	private String email;
	private String status;
}
