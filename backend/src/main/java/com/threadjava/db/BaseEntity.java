package com.threadjava.db;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;
import java.util.UUID;

@Data
@MappedSuperclass
public class BaseEntity {

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id",nullable = false, updatable = false)
	private UUID id;

	@Column(name = "timestamp", nullable = false, updatable = false)
	@CreationTimestamp
	private Date createdAt;

	@Column(name = "updated_on", nullable = false)
	@UpdateTimestamp
	private Date updatedAt;

}
